package 接口;

public class Baby {

	// 饥饿度 0~100
	int hungry;
	
//	Mother myMother;
//	Father myFather;
	// 需要一个喂养我的人
	Robot feedMan;
	

	public int getHungry() {
		return hungry;
	}

	public void setHungry(int hungry) {
		this.hungry = hungry;
		
		if(hungry>60)
		{
//			System.out.println("婴儿:妈妈，我饿了~~~~~");
//			myMother.feed(this);
//			System.out.println("婴儿:爸爸，我饿了~~~~~");
//			myFather.feed(this);
			
			System.out.println("婴儿:我饿了~~~~~");
			feedMan.feed(this);
			
		}
		
	}
	
	public boolean isAlive()
	{
		if(this.hungry>=100)
		{
			System.out.println("婴儿:饿死了");
			return false;
		}
		return true;
	}
	
}