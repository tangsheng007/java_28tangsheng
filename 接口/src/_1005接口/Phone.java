package _1005接口;

public class Phone implements IPlayGame{
	
	String brand;
	int price;
	IPlayGame play;
	
	public void call()
	{
		System.out.println("在使用" + price + "元手机" + brand + "打电话");
	}
	
	@Override
	public void playGame() {

		System.out.println("在使用" + price + "元手机" + brand + "发短信");
		
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public Phone() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Phone(String brand, int price) {
		super();
		this.brand = brand;
		this.price = price;
	}
	
	
	
}