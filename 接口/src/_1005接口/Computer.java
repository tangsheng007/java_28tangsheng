package _1005接口;

public class Computer implements IPlayGame{
	
	String brand;
	int price;
	
	
	public void playGame() {
		System.out.println("使用" + price + "元电脑" + brand + "玩游戏");
		
	}
	
	public void coding()
	{
		System.out.println("使用" + price + "元电脑" + brand + "开发JavaEE应用");
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public Computer() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Computer(String brand, int price) {
		super();
		this.brand = brand;
		this.price = price;
	}

	
	
	
	
	
}
