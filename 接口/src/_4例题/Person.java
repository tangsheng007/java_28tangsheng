package _4例题;

public class Person {
	
	String name;
	IFindAble findMan;
	
	public boolean askForHouse()
	{
		// 打电话给中介公司或者朋友，询问房源信息
		
		System.out.println("打电话询问房源信息");
		
		int price = findMan.find();
		
		if(price==1000)
		{
			System.out.println("这个房子我要了");
			return true;
		}
		else
		{
			System.out.println("请再帮我找一找");
			return false;
		}
		
		
	}
}
