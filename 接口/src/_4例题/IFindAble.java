package _4例题;

//找房子接口
public interface IFindAble {

	// 返回一个房源的价格信息
	public int find();
}