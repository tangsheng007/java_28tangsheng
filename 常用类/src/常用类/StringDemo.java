package 常用类;

import java.util.Arrays;
import java.util.Scanner;

public class StringDemo {

	public static void main(String[] args) 
	{
	
		
		// 字符串类 
		// 不可变字符串
		// String 
		// StringBuilder
		// StringBuffer 可变字符串
		
		// 1. 创建字符
//		create();
		
		
		// 2. 常用方法
//		input();
		
		// 3. 判断
//		compare();
		
		// 4. 转换
//		trans();
		
		// 5. 操作(重要,需要掌握)
		op();
		
	}

	private static void op() {
		
		// 1. 字符串查找
		String s1 = "1qs;dfabcjsdf23049sadfasedfqaser12435rasdfasedfasdfabc";
		System.out.println(s1.indexOf("abc"));// -1 表示没有
		System.out.println(s1.indexOf("abc", 20));// 从20位置继续往后找
		
		System.out.println(s1.lastIndexOf("abc"));// 从后往前找
		System.out.println(s1.lastIndexOf("abc",40));// 从后往前找
		
		
		// 2. 替换
		System.out.println(s1.replace("abc", "XXXX"));
		
		// 3. 分割
		String s2 = "java,c++,c#,Swift";
		String[] strs = s2.split(",");
		System.out.println(Arrays.toString(strs));
		
		// 4. 截取
		String s3 = "abcdefghijk";
		System.out.println(s3.substring(5));
		System.out.println(s3.substring(5,5+4));
		
	}

	private static void trans() {
		// TODO Auto-generated method stub
		
		// 数字转换
		String str = "123";
		int num = Integer.parseInt(str);
		
		// 转换为字符数组
		char[] cs = str.toCharArray();
		
		
	}

	private static void compare() {


		System.out.println("a".compareTo("b"));// -1
		System.out.println("aa".compareTo("ac"));// -2

		System.out.println("Demo1.java".startsWith("Demo"));
		System.out.println("Demo1.java".endsWith(".java"));
	}

	private static void input() {
		Scanner sc = new Scanner(System.in);
		
		String name1 = sc.next();// 遇到空格会结束
//		String name1 = sc.nextLine();// 接受一整行
		
		System.out.println(name1);
		
		System.out.println("长度:"+name1.length());
		
		System.out.println(name1.charAt(0));
		
		
	}

	private static void create() 
	{
		String str1 = "abc";
		String str2 = ""; // 长度为0的字符串
		String str3 = new String("abc");// 堆内存新建了一个String对象
		String str4 = null;// 没有指向任何字符串对象
		
		String str5 = "abc";
		System.out.println(str1 == str5);// true 他们都指向常量池中的那个"abc"
		System.out.println(str3 == str1);// false 
		
		// "abc" 字符串常量 会放在一个字符串常量池中
		
		// 比较字符串相等一定要用equals方法不要用==
		
		byte[] buf1 = new byte[]{104,111,109,101};// ASCII
		String str6 = new String(buf1);
		System.out.println(str6);
		
		
		char[] buf2 = new char[] {'h','o','m','e'};
		String str7 = new String(buf2);
		System.out.println(str7);
		
		
		
		
	}

}
