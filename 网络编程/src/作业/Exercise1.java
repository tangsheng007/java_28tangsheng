package 作业;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;

public class Exercise1
{

// 1. 找一个网页获取无锡天气信息，从网页的字符串中解析出今天的天气和气温
	public static void main(String[] args)
	{
		try
		{
			// 101190201江苏无锡市
			URL url = new URL("http://t.weather.itboy.net/api/weather/city/101190201");
			// 打开网址
			InputStreamReader isReader = new InputStreamReader(url.openStream(), "UTF-8");
			BufferedReader br = new BufferedReader(isReader);
			String str;
			while ((str = br.readLine()) != null)
			{
				String regex = "\\p{Punct}+";
				String digit[] = str.split(regex);
				System.out.println('\n' + "城市:" + digit[22] + digit[18]);

				System.out.println('\n' + "温度:" + digit[47] + "~" + digit[45]);

		
			}
			// 关闭流
			br.close();
			isReader.close();
		} catch (Exception exp)
		{
			System.out.println(exp);
		}
	}
}