package URL;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;

public class TestDemo {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub

		// URL类
		
		
		// TCP编程
		// UDP编程
		
		
//		URL url = new URL("https://www.baidu.com");
//		URL url = new URL("https://www.qq.com");
		URL url = new URL("https://tianqi.2345.com/wuxi/58354.htm");
		
		// url.openStream() 打开一个字节流流读取网址内容
		// InputStreamReader  将字节流 转换为 字符流
		// 打开网址
		Reader r = new InputStreamReader(url.openStream(),"utf-8");
//		Reader r = new InputStreamReader(url.openStream(),"gbk");
		// 读取内容
		char[] buf = new char[1024];
		int len = 0;
		StringBuilder sb = new StringBuilder();
		while((len=r.read(buf))!=-1)
		{
			sb.append(buf,0,len);
		}
		System.out.println(sb.toString());
		// 关闭流
		r.close();
		
	}

}