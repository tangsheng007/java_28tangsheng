package 方法;

public class zuidazhi {

	public static void main(String[] args) {
		// TODO 自动生成的方法存根
		
		System.out.println(max(100,200));
		System.out.println(max(100,200,300));
		System.out.println(max(100.5,200.3));
		System.out.println(max(100.5f,200.4f));

	}
	public static int max(int x,int y)  // 传入两个int，返回最大的
	{
		if(x>y)
		{
			return x;
		}
		else
		{
			return y;
		}
	}
	
	public static int max(int x,int y,int z) // 传入三个int，返回最大的
	{
		if(x>y)
		{
			if(x>z)
			{
				return x;
			}
			else
			{
				return z;
			}
		}
		else
		{
			if(y>z)
			{
				return y;
			}
			else
			{
				return z;
			}
		}
	}
	
	public static double max(double x,double y)  // 传入两个double，返回最大的
	{
		if(x>y)
		{
			return x;
		}
		else
		{
			return y;
		}
	}
	
	public static float max(float x,float y)  // 闯入两个float,返回最大的
	{
		if(x>y)
		{
			return x;
		}
		else
		{
			return y;
		}
	}

}
