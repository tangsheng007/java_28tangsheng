package 抽象类2;

public class Rect extends Shape {
	int chang;
	int kuan;
	
	public void perimeter() {
		System.out.println("周长："+(chang+kuan)*2);
	}
	
	public void area() {
		System.out.println("面积："+(chang*kuan));
	}

	public int getChang() {
		return chang;
	}

	public void setChang(int chang) {
		this.chang = chang;
	}

	public int getKuan() {
		return kuan;
	}

	public void setKuan(int kuan) {
		this.kuan = kuan;
	}

	public Rect(int chang, int kuan) {
		super();
		this.chang = chang;
		this.kuan = kuan;
	}

	public Rect() {
		super();
		// TODO Auto-generated constructor stub
	}
	

}
