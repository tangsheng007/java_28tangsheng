package 抽象类1;

public class TestDome {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		// 练习:
		// 定义一个员工类Employee(抽象类)
		// 属性: 名字name
		// 方法: 工作work()   (定义成抽象方法)
		// 定义一个收银员类CashEmployee从Employee继承
		//    重写work()方法
		// 定义一个销售员类SellEmployee从Employee继承
		//    重写work()方法
		
		// 创建2个员工，并测试
		Employee e1 = new Cash();
		e1.work();
		Employee e2 = new Sell();
		e2.work();

	}

}
