package _0708zuoye;

import java.util.Scanner;

import static java.lang.Math.*;
/*用户输入圆半径，计算圆的周长和面积，并打印输出*/
public class Yuan {

	public static void main(String[] args) {
		// TODO 自动生成的方法存根
		
			Scanner scanner = new Scanner(System.in);
			System.out.println("请输入圆的半径： ");
			double radius = scanner.nextDouble();
			System.out.println("圆的周长为: "+radius*2*PI);
			System.out.println("圆的面积为: "+pow(radius, 2)*PI);
		}

}
