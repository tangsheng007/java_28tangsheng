package 练习;

import java.util.Vector;

public class Exercise1 {
	
	public static String genPass = null;
	public static Vector<String> passAll = new Vector<>();

	public static void main(String[] args) throws InterruptedException {
		Exercise1.genPass = generateCode(2);
		System.out.print(Exercise1.genPass);
		Thread.sleep(3000);
		
		CrackPassRunnable r = new CrackPassRunnable();
		Thread t = new Thread(r);
		
		PrintRunnable p = new PrintRunnable();
		Thread t2 = new Thread(p);
		t2.setDaemon(true);
		
		t.start();
		t2.start();
		
	}

	public static String generateCode(int n) {


		// 然后产生一个长度为n随机的字符串(含小写、大写、数字)
		char[] cs = new char[62];// 26+26+10
		for (int i = 0; i < 26; i++) {
			cs[i] = (char) ('a' + i);
			cs[i + 26] = (char) ('A' + i);
		}
		for (int i = 0; i < 10; i++) {
			cs[51 + i] = (char) ('0' + i);
		}
		String str = new String(cs);
//		System.out.println(str);

		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < n; i++) {
			sb.append(str.charAt((int) (Math.random() * str.length())));
		}
//		System.out.println(sb.toString());
		
		return sb.toString();
	}

}
