package 线程控制;

public class DaemoDemo {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		
		DaemonThread t1=new DaemonThread();
		t1.setName("袭人");
		t1.setDaemon(true);// 设定为守护线程，一旦主线程结束，守护线程也终止
		t1.start();
		
		for(int i=0;i<30;i++)
		{
			System.out.println("贾宝玉 睡觉中zzz");
			Thread.sleep(300);
			System.out.println("贾宝玉 起床了");
		}

	}

}
class DaemonThread extends Thread
{
	public void run() {
		while(true) {
			System.out.println(this.getName()+"扇扇子");
			try {
				Thread.sleep(300);
			}catch(InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
