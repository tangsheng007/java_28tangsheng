package 线程同步2;

public class SellerRunnable implements Runnable
{
	int count = 100;

	@Override
	public void run() 
	{
		while(count>0)
		{
			sellOne();
			
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	// 方式二: 在方法前添加修饰符 synchronized
	// 这种方法称之为同步方法
	// 同一时间只有一个线程能执行
	synchronized public void sellOne()
	{
		count--;
		System.out.println(Thread.currentThread().getName() + "卖出一张票,剩余:"+count);
	}

}