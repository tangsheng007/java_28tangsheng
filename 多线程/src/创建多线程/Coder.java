package 创建多线程;

public class Coder {
	
	String name;
	
	public void code() throws InterruptedException {
		System.out.println("上班中 开始写代码");
		for(int i=0;i<8*60;i++)
		{
			System.out.println(name+":写代码中，写了"+i/60+"小时");
			Thread.sleep(10);
		}
		System.out.println("写完代码，下班");
	}
	
	public void listen() throws InterruptedException {
		System.out.println("带上耳机 开始听音乐");
		for(int i=0;i<8*60;i++)
		{
			System.out.println(name+":听音乐中，听了"+i/60+"小时");
			Thread.sleep(10);
		}
		System.out.println("听完音乐，摘下耳机");
	}

}
