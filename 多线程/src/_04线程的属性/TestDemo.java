package _04线程的属性;

public class TestDemo {

	public static void main(String[] args) 
	{
	
		// 创建线程对象
		MyThread1 t = new MyThread1();
		t.setName("子线程1");
		t.setPriority(1);// 1~10
		
		// 创建线程对象
		MyThread1 t2 = new MyThread1();
		t2.setPriority(10);
		// 启动线程
		// 创建线程对象
		MyThread1 t3 = new MyThread1();
		// 启动线程
		
		// 启动线程
		t.start();
		t2.start();		
		t3.start();
		
		
//		线程编号:14
//		线程名字:Thread-0
//		线程优先级:5
		
//		线程编号:15
//		线程名字:Thread-1
//		线程优先级:5		
		
		
		// 主线程
		// Thread.currentThread() 得到当前运行这行代码的线程对象
		System.out.println("主线程id:"+Thread.currentThread().getId());
		System.out.println("主线程name:"+Thread.currentThread().getName());
		System.out.println("主线程优先级:"+Thread.currentThread().getPriority());
		
		

		// 线程id
		// 主线程  1  子线程编号10以上
		
		// 线程名字 可以修改的
		// 主线程main  子线程 thread-0
		
		// 优先级 1~10
		// 默认5 理论上优先极高优先执行
	}

}