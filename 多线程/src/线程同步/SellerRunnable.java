package 线程同步;

public class SellerRunnable implements Runnable{
	
	int count=100;

	@Override
	public void run() {
		{
			while(count>0)
			{
				//使用synchronized 修饰代码
				//同一时间内，只有一个线程能够执行内部的代码
				//第一个线程先执行到这段代码，它就锁住括号中的代码，执行这里的代码
				//第二个线程执行到这段代码，发现对象被锁住了，
				synchronized(this)
				{
					count--;
					System.out.println(Thread.currentThread().getName()+"卖出一张票，剩余："+count);
				}
				
				try {
					Thread.sleep(50);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		}
		
	}

}