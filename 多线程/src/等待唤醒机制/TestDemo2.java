package 等待唤醒机制;

import java.util.*;


//wait()		进入阻塞状态
//notify()		唤醒其他线程
//notifyAll()	唤醒其他线程 (多生产者，多消费者)



public class TestDemo2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// 小明吃包子
		// 妈妈做包子	
		
		
		// 锅
		Pot pot = new Pot();
		
		// 妈妈
		Productor p = new Productor();
		p.pot = pot;
		Thread t1 = new Thread(p);
		t1.setName("妈妈");
		t1.start();
		
		// 小明
		Customer c = new Customer();
		c.pot = pot;
		Thread t2 = new Thread(c);
		t2.setName("小明");
		t2.start();
		
		// 大明
		Thread t3 = new Thread(c);
		t3.setName("大明");
		t3.start();
		
	}

}

//锅
class Pot
{
	// 容器
	public Queue<Integer> queue = new LinkedList<>();
	// 最大容量
	public int maxSize = 10;
	// 记录包子编号
	public int n = 0;
}

class Productor implements Runnable
{
	Pot pot;

	@Override
	public void run() 
	{
		// TODO Auto-generated method stub
		while(true)
		{
			synchronized (pot) {
				
				
				if(pot.queue.size()<pot.maxSize)
				{
					// 做包子
					pot.n++;
					// 放入锅里
					pot.queue.add(pot.n);
					System.out.println(Thread.currentThread().getName()+"做包子"+pot.n+"放到了锅里");
				}
				else
				{
					System.out.println(Thread.currentThread().getName()+"锅满了,不用再做了");
					
					// 发出通知
//					pot.notify();
					pot.notifyAll();
					
					// 进入等待状态
					try {
						pot.wait();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}
			}
			
			
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	
}


class Customer implements Runnable
{
	Pot pot;
	
	@Override
	public void run()
	{
		while(true)
		{
			synchronized (pot) {
				
				if(pot.queue.size()>0)
				{
					// 取出包子
					int n = pot.queue.remove();
					System.out.println(Thread.currentThread().getName()+"吃包子,包子编号:"+n);
				}
				else
				{
					System.out.println(Thread.currentThread().getName()+"锅是空的，妈妈继续做包子");
					
					// 通知妈妈做包子
					pot.notifyAll();
					
					while(pot.queue.size()<1)
					{
						// 进入等待
						try {
							pot.wait();
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					
					
				}
				
			}
			
			
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
	}
	
}