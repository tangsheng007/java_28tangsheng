package 线程池;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class TestDemo {
	
	public static void main(String[] args) throws InterruptedException, ExecutionException {
		//创建一个线程池
//		ExecutorService pool=Executors.newFixedThreadPool(5);
//		//卖票的任务
//		SellerRunnable s=new SellerRunnable();
//		
//		pool.submit(s);
//		pool.submit(s);
//		pool.submit(s);
//		pool.submit(s);
//		pool.submit(s);
		
		ExecutorService pool=Executors.newFixedThreadPool(5);
		MyCallable c=new MyCallable();
		//提交任务
		Future<String> f1=pool.submit(c);
		Future<String> f2=pool.submit(c);
		
		System.out.println("主线程等待计算结果：");
		String result1=f1.get();
		String result2=f2.get();
		System.out.println("主线程等到结果出来了：");
		System.out.println(result1);
		System.out.println(result2);
	}

}
