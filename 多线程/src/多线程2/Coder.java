package 多线程2;

public class Coder implements Runnable{
	
	String name;
	
	public void code()
	{
		System.out.println("上班中 开始写代码");
		for(int i=0;i<8*60;i++)
		{
			System.out.println(name+":写代码中,写了"+i/60+"小时");
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("写完代码，下班!");
	}
	
	
	public void listen()
	{
		System.out.println("带上耳机 开始听音乐");
		for(int i=0;i<8*60;i++)
		{
			System.out.println(name+":听音乐中,听了"+i/60+"小时");
			
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("听完音乐,摘下耳机");
	}


	@Override
	public void run() {
		
		listen();
	}

}