package 多线程2;

public class TestDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// 方式2
		// 实现Runnable接口 重写run方法
		
		Coder c = new Coder();
		c.name = "小明";
		
		// 创建一个线程对象
		Thread t = new Thread(c);
		// 在子线程中执行听音乐
		t.start();
		
		c.code(); // 8个小时
	}

}
