package yunsuanfu;

public class RelationshipOperatorDemo {

	public static void main(String[] args) {
		// TODO 自动生成的方法存根
		
		//&
		//|
		//^
		//!
		//&与	两边都为true
		System.out.println(true & true);
		System.out.println(false & true);
		System.out.println(true & false);
		System.out.println(false & false);
		
		//| 或	只要一个条件true
		System.out.println(true | true);
		System.out.println(false | true);
		System.out.println(true | false);
		System.out.println(false | false);
		
		//^ 异或
		System.out.println(true ^ true);
		System.out.println(false ^ true);
		System.out.println(true ^ false);
		System.out.println(false ^ false);
		
		//!	取反
		System.out.println(!false);
		System.out.println(!true);
		
		int i=5;
		int j=4;
		boolean result=(i<4)&(j++>3);//false & true
		
		System.out.println("result:"+result);//false
		System.out.println(i);
		System.out.println(j);
		
		
	}

}
