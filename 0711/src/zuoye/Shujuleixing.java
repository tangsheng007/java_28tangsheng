package zuoye;

public class Shujuleixing {

	public static void main(String[] args) {
		// TODO 自动生成的方法存根
		
		/*1. java中的8种基本数据类型，根据其占⽤内存空间大小，有小到大依次是（写类型关键字）。*/
		//boolean(布尔型) 1个字节
		//byte(字节型) 占用1字节
		//short(短整型) 占用2个
		//char(字符型) 占用2字节
		//int(整型) 占用4个字节
		//float(浮点型) 4个字节
		//long(长整型) 8个字节
		//double(双精度浮点型) 8个字节
		
		
		int a = 5;
		int b = 6;
		if (a>3||b++<6){// || 短路或 当第一个条件为true 第二的条件不会执⾏
		a--;//	a-1
		}
		System.out.println(a+","+b);
		
		//输出结果为4,6

	}

}