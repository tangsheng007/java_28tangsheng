package 学生管理系统;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import _1_0.Student;

import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class AddFrame extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	
	
	MainFrame mainFrame;




	/**
	 * Create the frame.
	 */
	public AddFrame(MainFrame mainFrame)
	{
		this.mainFrame = mainFrame;
		
		
		setTitle("添加员工");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 235, 299);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		textField = new JTextField();
		textField.setColumns(10);
		textField.setBounds(81, 27, 66, 21);
		contentPane.add(textField);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(81, 57, 66, 21);
		contentPane.add(textField_1);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(81, 88, 66, 21);
		contentPane.add(textField_2);
		
		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(81, 117, 66, 21);
		contentPane.add(textField_3);
		
		textField_4 = new JTextField();
		textField_4.setColumns(10);
		textField_4.setBounds(81, 155, 66, 21);
		contentPane.add(textField_4);
		
		JButton btnNewButton = new JButton("添加");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				// 1. 获取填写的信息
				int id = Integer.parseInt(textField.getText());
				String name = textField_1.getText();
				int chinese = Integer.parseInt(textField_2.getText());
				int math = Integer.parseInt(textField_3.getText());
				int english = Integer.parseInt(textField_4.getText());
				
				// 2. 一个员工放入数组
				Student stu = new Student(id,name,chinese,math,english);
				for(int i=0;i<100;i++)
				{
					if(mainFrame.stus[i]==null)
					{
						mainFrame.stus[i] = stu;
						break;
					}
				}
				
				// 3. 刷新一下表格显示
				
				mainFrame.refreshTable();
				
				// 隐藏添加窗体
				AddFrame.this.setVisible(false);
				
			}
		});
		btnNewButton.setBounds(81, 196, 57, 23);
		contentPane.add(btnNewButton);
		
		JLabel lblNewLabel = new JLabel("工号");
		lblNewLabel.setBounds(10, 30, 54, 15);
		contentPane.add(lblNewLabel);
	}
}