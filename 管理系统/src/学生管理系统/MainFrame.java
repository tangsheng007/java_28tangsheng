package 学生管理系统;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import _1_0.Student;

import javax.swing.JTable;
import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;


// 1. 显示员工列表 显示到表格当中去
// 2. 增加功能

public class MainFrame extends JFrame {

	private JPanel contentPane;
	public JTable table;
	
	// 存储100个员工信息
	Student[] stus = new Student[100];
	private JTextField idField;
	private JTextField nameField;
	private JTextField chineseField;
	private JTextField englishField;
	private JTextField mathField;
	private JMenuBar menuBar;
	private JMenu mnNewMenu;
	private JMenuItem mntmNewMenuItem;
	private JMenuItem mntmNewMenuItem_1;
	private JMenu mnNewMenu_1;
	private JMenuItem mntmNewMenuItem_2;
	private JMenuItem mntmNewMenuItem_3;
	
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainFrame frame = new MainFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 494, 381);
		
		menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		mnNewMenu = new JMenu("New menu");
		menuBar.add(mnNewMenu);
		
		mntmNewMenuItem = new JMenuItem("New menu item");
		mnNewMenu.add(mntmNewMenuItem);
		
		mntmNewMenuItem_1 = new JMenuItem("New menu item");
		mnNewMenu.add(mntmNewMenuItem_1);
		
		mnNewMenu_1 = new JMenu("员工管理");
		menuBar.add(mnNewMenu_1);
		
		mntmNewMenuItem_2 = new JMenuItem("添加员工");
		mnNewMenu_1.add(mntmNewMenuItem_2);
		mntmNewMenuItem_2.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				
				// 弹一个窗体让用户输入员工数据
				
				AddFrame a = new AddFrame(MainFrame.this);
				a.setVisible(true);
				
				
			}
			
		});
		
		mntmNewMenuItem_3 = new JMenuItem("删除员工");
		mnNewMenu_1.add(mntmNewMenuItem_3);
		mntmNewMenuItem_3.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				// 1. 获取到用户当前选择的哪一行
				System.out.println(table.getSelectedRow());
								
				// 2. 从数组中移除
				stus[table.getSelectedRow()] = null;
				
				// 3. 刷新表格
				refreshTable();
				
			}
		});
		
		
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		setContentPane(contentPane);
		
		// 添加一些学生数据
		stus[0] = new Student(1,"张三",100,90,70);
		stus[1] = new Student(2,"李四",100,90,70);
		stus[2] = new Student(3,"王五",100,90,70);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		table = new JTable();
		contentPane.add(table);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.SOUTH);
		
		idField = new JTextField();
		panel.add(idField);
		idField.setColumns(10);
		
		nameField = new JTextField();
		panel.add(nameField);
		nameField.setColumns(10);
		
		chineseField = new JTextField();
		panel.add(chineseField);
		chineseField.setColumns(10);
		
		mathField = new JTextField();
		panel.add(mathField);
		mathField.setColumns(10);
		
		englishField = new JTextField();
		panel.add(englishField);
		englishField.setColumns(10);
		
		JButton btnNewButton = new JButton("添加");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				// 1. 获取填写的信息
				int id = Integer.parseInt(idField.getText());
				String name = nameField.getText();
				int chinese = Integer.parseInt(chineseField.getText());
				int math = Integer.parseInt(mathField.getText());
				int english = Integer.parseInt(englishField.getText());
				
				// 2. 一个学生放入数组
				Student stu = new Student(id,name,chinese,math,english);
				for(int i=0;i<100;i++)
				{
					if(stus[i]==null)
					{
						stus[i] = stu;
						break;
					}
				}
				
				// 3. 刷新一下表格显示
				
				refreshTable();
			}
		});
		panel.add(btnNewButton);
		
		refreshTable();
	}
	
	public void refreshTable()
	{
		String[] cols = {"学号","姓名","语文","数学","外语"};
		
		String[][] datas = new String[100][5];
		int tmp = 0;
		for(int i=0;i<100;i++)
		{
			if(stus[i]!=null)
			{
				datas[tmp][0] = ""+stus[i].getId();
				datas[tmp][1] = ""+stus[i].getName();
				datas[tmp][2] = ""+stus[i].getChinese();
				datas[tmp][3] = ""+stus[i].getMath();
				datas[tmp][4] = ""+stus[i].getEnglish();
				tmp++;
			}
		}
		TableModel tm = new DefaultTableModel(datas,cols);
		table.setModel(tm);
	}

}