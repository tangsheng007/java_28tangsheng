package _1_0;

import java.util.Scanner;

//学生管理系统
public class SchoolSystem {

	// 属性
	
	// 存储100个学生信息
	Student[] stus = new Student[100];
	
	Scanner sc = new Scanner(System.in);
	
	// 主流程
	public void run()
	{
		System.out.println("欢迎使用xxx学校管理系统");
		
		while(true)
		{
			// 打印菜单
			System.out.println("1. 添加学生");
			System.out.println("2. 列出学生");
			System.out.println("3. 删除学生");
			System.out.println("4. 修改学生");
			System.out.println("9. 退出系统");
			
			// 让用户选择
			System.out.println("请输入要执行的操作:");
			int sel = sc.nextInt();
			
			if(sel==9) break;
			
			switch(sel)
			{
			case 1:
				add();
				break;
			case 2:
				list();
				break;
			case 3:
				del();
				break;
			case 4:
				modify();
				break;
			}
		}
		
		System.out.println("程序关闭中，欢迎在再次使用");
		
		
	}
	
	private void modify() {
		System.out.println("修改学生信息");
		
		System.out.println("请输入要修改的学生的编号:");
		int id = sc.nextInt();
		
		int index = -1;
		for(int i=0;i<100;i++)
		{
			if(stus[i] != null)
			{
				if(stus[i].getId() == id)
				{
					// 记录要修改的学生索引
					index = i;
					break;
				}
			}
		}
		
		if(index!=-1)
		{
			System.out.println("请输入学生的新信息:");
			String name = sc.next();
			int chinese = sc.nextInt();
			int math = sc.nextInt();
			int english = sc.nextInt();
			stus[index].setName(name);
			stus[index].setChinese(chinese);
			stus[index].setMath(math);
			stus[index].setEnglish(english);
			System.out.println("修改成功");
		}
		else
		{
			System.out.println("没有找到要修改的学生");
		}
	}

	private void del() {
		
		System.out.println("删除学生信息");
		System.out.println("请输入要删除学生的编号:");
		int id = sc.nextInt();
		
		boolean finded = false;
		for(int i=0;i<100;i++)
		{
			if(stus[i] != null)
			{
				if(stus[i].getId() == id)
				{
					stus[i] = null;
					System.out.println("删除成功!!");		
					finded = true;
					break;
				}
			}
		}
		if(!finded)
		{
			System.out.println("没有找到这个学号的学生!!");
		}
	}

	public void add()
	{
		System.out.println("添加学生功能");
		
		// 1. 输入学生信息
		int id = sc.nextInt();
		String name = sc.next();
		int chinese = sc.nextInt();
		int math = sc.nextInt();
		int english = sc.nextInt();
		// 2. 放入学生对象
		Student stu = new Student(id,name,chinese,math,english);
		
		// 3. 把学生对象放入数组
		for(int i=0;i<100;i++)
		{
			if(stus[i] == null)
			{
				stus[i] = stu;
				break;
			}
		}
		
	}
	
	public void list()
	{
		System.out.println("列表学生功能");
		System.out.println("id\t姓名\t语文\t数学\t外语");
		for(int i=0;i<100;i++)
		{
			if(stus[i]!=null)
			{
				System.out.println(stus[i]);
			}
		}
	}

}