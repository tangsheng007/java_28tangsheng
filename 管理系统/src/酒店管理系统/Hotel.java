package �Ƶ����ϵͳ;

public class Hotel {
	private int id;
	private String name;
	private char sex;
	private String idname;
	private String money;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMoney() {
		return money;
	}
	public void setMoney(String money) {
		this.money = money;
	}
	
	public char getSex() {
		return sex;
	}
	public void setSex(char sex) {
		this.sex = sex;
	}
	public String getIdname() {
		return idname;
	}
	public void setIdname(String idname) {
		this.idname = idname;
	}
	
	
	public Hotel() {
		super();
		
	}
	
	
	public Hotel(int id, String name, char sex, String idname, String money) {
		super();
		this.id = id;
		this.name = name;
		this.sex = sex;
		this.idname = idname;
		this.money = money;
	}
	@Override
	public String toString() {
		return "Hotel [id=" + id + ", name=" + name + ", sex=" + sex + ", idname=" + idname + ", money=" + money + "]";
	}
		
	
	

}
