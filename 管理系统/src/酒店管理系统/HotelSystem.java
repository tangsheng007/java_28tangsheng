package 酒店管理系统;

import java.util.Scanner;

public class HotelSystem {
	
	// 存储50个学生信息
	Hotel[] stus=new Hotel[50];
	
	Scanner sc=new Scanner (System.in);

	// 主流程
		public void run()
		{
			System.out.println("欢迎使用酒店管理系统");
			
			while(true)
			{
				// 打印菜单
				System.out.println("1. 添加客户");
				System.out.println("2. 列出客户");
				System.out.println("3. 删除客户");
				System.out.println("4. 修改客户");
				System.out.println("9. 退出系统");
				
				// 让用户选择
				System.out.println("请输入要执行的操作:");
				int sel = sc.nextInt();
				
				if(sel==9) break;
				
				switch(sel)
				{
				case 1:
					add();
					break;
				case 2:
					list();
					break;
				case 3:
					del();
					break;
				case 4:
					modify();
					break;
			}
		}
			System.out.println("程序关闭");	
		}
		
		private void list() {
			
		}
		
		private void del() {
			
		}
		private void modify() {
			System.out.println("修改客户信息");
			System.out.println("输入要修改的客户的订单号:");
			
			int id = sc.nextInt();
			int index = -1;
			for(int i=0;i<50;i++)
			{
				if(stus[i] != null)
				{
					if(((Hotel) stus[i]).getId() == id)
					{
						// 记录要修改的学生索引
						index = i;
						break;
					}
				}
			}
			
			if(index!=-1)
			{
				System.out.println("请输入客户的信息:");
				String name = sc.next();
				char sex = sc.next().charAt(0);
				String idname= sc.next();
				String money = sc.next();
				stus[index].setName(name);
				stus[index].setIdname(idname);
				stus[index].setSex(sex);
				stus[index].setMoney(money);
				stus[index].setId(id);
				System.out.println("修改成功");
			}
			else
			{
				System.out.println("没有找到要修改的客户");
			}
		}
		private void add() 
		{
			System.out.println("添加客户信息功能");
			// 1. 输入客人信息
			int id = sc.nextInt();
			String idname = sc.next();
			String name = sc.next();
			String money  = sc.next();
			char sex = sc.next().charAt(0);
			
			// 2. 放入客人对象
			Hotel h = new Hotel(id,name,sex,idname,money);
			
			// 3. 将客人对象放入数组
			for(int i=0;i<100;i++)
			{
				if(stus[i] != null)
				{
					stus[i] = h;
					break;
				}
			}
		}

}
		
