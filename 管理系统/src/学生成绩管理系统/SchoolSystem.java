package 学生成绩管理系统;

import java.util.Scanner;

//学生管理系统
public class SchoolSystem {

	// 属性
	
	// 存储100个学生信息
	Student[] stus = new Student[100];
	
	Scanner sc = new Scanner(System.in);
	
	// 主流程
	public void run()
	{
		System.out.println("欢迎使用xxx学校管理系统");
		
		while(true)
		{
			// 打印菜单
			System.out.println("1. 添加学生");
			System.out.println("2. 列出学生");
			System.out.println("9. 退出系统");
			
			// 让用户选择
			System.out.println("请输入要执行的操作:");
			int sel = sc.nextInt();
			
			if(sel==9) break;
			
			switch(sel)
			{
			case 1:
				add();
				break;
			case 2:
				list();
				break;
			}
		}
		
		System.out.println("程序关闭中，欢迎在再次使用");
		
		
	}
	
	public void add()
	{
		System.out.println("添加学生功能");
		
		// 1. 输入学生信息
		int id = sc.nextInt();
		String name = sc.next();
		int chinese = sc.nextInt();
		int math = sc.nextInt();
		int english = sc.nextInt();
		// 2. 放入学生对象
		Student stu = new Student(id,name,chinese,math,english);
		
		// 3. 把学生对象放入数组
		for(int i=0;i<100;i++)
		{
			if(stus[i] == null)
			{
				stus[i] = stu;
				break;
			}
		}
		
	}
	
	public void list()
	{
		System.out.println("列表学生功能");
		System.out.println("id\t姓名\t语文\t数学\t外语");
		for(int i=0;i<100;i++)
		{
			if(stus[i]!=null)
			{
				System.out.println(stus[i]);
			}
		}
	}

}