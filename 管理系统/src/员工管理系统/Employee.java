package 员工管理系统;

public abstract class Employee {

	// 工号

	protected int id;

	// 姓名

	protected String name;

	// 年龄

	protected int age;

	// 工种

	protected String profession;

	// 工龄

	protected int workAge;

	// 基本工资

	protected double salary;

	// 奖金

	protected double bouns;



	// 抽象方法

	public abstract void work();



	// 计算工资

	public double calMoney() {



		return this.salary + this.bouns;

	}



	public int getId() {

		return id;

	}



	public void setId(int id) {

		this.id = id;

	}



	public String getName() {

		return name;

	}



	public void setName(String name) {

		this.name = name;

	}



	public int getAge() {

		return age;

	}



	public void setAge(int age) {

		this.age = age;

	}



	public String getProfession() {

		return profession;

	}



	public void setProfession(String profession) {

		this.profession = profession;

	}



	public double getSalary() {

		return salary;

	}



	public void setSalary(double salary) {

		this.salary = salary;

	}



	public double getBouns() {

		return bouns;

	}



	public void setBouns(double bouns) {

		this.bouns = bouns;

	}



	public int getWorkAge() {

		return workAge;

	}



	public void setWorkAge(int workAge) {

		this.workAge = workAge;

	}



	//有参

	public Employee(int id, String name, int age, int workAge, String profession, double salary, double bouns) {

		super();

		this.id = id;

		this.name = name;

		this.age = age;

		this.profession = profession;

		this.salary = salary;

		this.bouns = bouns;

		this.workAge = workAge;

	}



	//无参

	public Employee() {

		super();

		// TODO Auto-generated constructor stub

	}



	@Override

	public String toString()

	{

		return "" + id + "\t" + name + "\t"+ profession+"\t" + salary + "\t" + bouns + "\t\t\t" + calMoney();

	}



}

