package 员工管理系统;

import java.util.Scanner;



import 员工管理系统.Driver;

import 员工管理系统.Employee;

import 员工管理系统.IDriverAble;

import 员工管理系统.Manager;

import 员工管理系统.Saler;



public class ManageSystem {



	//键盘输入

	Scanner sc = new Scanner(System.in);

	//创建数组

	Employee[] employees = new Employee[100];



	public void run() {



		System.out.println("欢迎使用xx公司员工管理系统");



		while (true) {

			printMenu();



			int sel = sc.nextInt();



			if (sel == 9) {

				break;

			}



			switch (sel) {

			case 1:

				

				//添加员工

				add();

				break;



			case 2:

				//删除员工

				del();

				break;



			case 3:

				//修改员工

				modify();

				break;



			case 4:

				//列表员工

				list();

				break;



			case 5:

				//所有员工工作

				allWork();

				break;



			case 6:

				//公司团建,让会开车的员开车

				groupWork();

				break;



			case 7:

				//临时司机开车

				tmpDriver();

				break;


			case 8:

				//全部员工工资

				setMoney();

				break;



			}



		}



	}

	


	//修改员工

	private void modify() {

		System.out.println("请输入要修改的员工工号:");

		int id = sc.nextInt();

		for (int i = 0; i < employees.length; i++) {

			if (id == employees[i].getId()) {

				System.out.println("请输入要修改的员工类型1经理2销售3司机:");

				int sel = sc.nextInt();

				System.out.println("请输入修改的员工姓名：");

				String name = sc.next();

				System.out.println("请输入修改的员工工资：");

				double salary = sc.nextDouble();

				System.out.println("请输入修改的员工奖金：");

				double bouns = sc.nextDouble();



				switch (sel) {

				case 1:

					employees[i] = new Manager(id, name, 0, 0, "", salary, bouns);

					break;

				case 2:

					System.out.println("请输入提成比率");

					double percent = sc.nextDouble();

					employees[i] = new Saler(id, name, 0, 0, "", salary, bouns, 0, percent);

					break;

				case 3:

					employees[i] = new Driver(id, name, 0, 0, "", salary, bouns);

					break;

				}



				System.out.println("修改成功!");

				break;



			}

		}

	}



	//删除员工

	private void del() {

		System.out.println("请输入员工姓名:");

		String name = sc.next();

		for (int i = 0; i < employees.length; i++) {

			if (name.equals(employees[i].getName())) // 字符串比较是否相同

			{

				employees[i] = null;

				System.out.println("删除成功");

				break;

			}

		}



	}



	//临时司机开车

	private void tmpDriver() {

		IDriverAble tmpDriver = new IDriverAble() {



			@Override

			public void dirve() {



				System.out.println("我是临时司机:带大家出去团建");

			}



		};

		tmpDriver.dirve();



	}

	



	//公司团建,让会开车的员开车

	private void groupWork() {

		System.out.println("公司团建,所有会开车的开车去");

		for (int i = 0; i < employees.length; i++) {

			if (employees[i] != null) {

				if (employees[i] instanceof IDriverAble) {

					System.out.println(employees[i].getName() + ":我会开车");

					IDriverAble d = (IDriverAble) employees[i];

					d.dirve();

				} else {

					System.out.println(employees[i].getName() + ":我不会开车");

				}

			}

		}



	}



	//所有员工工作

	private void allWork() {

		for (int i = 0; i < employees.length; i++) {

			if (employees[i] != null) {

				employees[i].work();

			}

		}



	}



	//添加员工

	private void add() {

		System.out.println("请输入要添加的员工类型1经理2销售3司机:");

		int sel = sc.nextInt();



		Manager m = new Manager();

		System.out.println("请输入员工工号:");

		int id = sc.nextInt();

		System.out.println("请输入员工姓名:");

		String name = sc.next();

		System.out.println("请输入员工工资:");

		double salary = sc.nextDouble();

		System.out.println("请输入员工奖金:");

		double bouns = sc.nextDouble();



		Employee e = null;

		switch (sel) {



		case 1:

			e = new Manager(id, name, 0, 0, "经理", salary, bouns);

			break;

		case 2:

			System.out.println("请输入提成比率:");

			double rate = sc.nextDouble();

			e = new Saler(id, name, 0, 0, "销售", salary, bouns, 0 , rate);

			break;

		case 3:

			e = new Manager(id, name, 0, 0, "司机", salary, bouns);

			break;

		}

		for (int i = 0; i < employees.length; i++) {

			if (employees[i] == null) {

				employees[i] = e;

				break;

			}

		}

	}



	//列表员工

	private void list() {



		System.out.println("工号\t姓名\t工种\t工资\t奖金\t业绩\t提成比率\t总收入");

		for (int i = 0; i < employees.length; i++) {

			if (employees[i] != null) {

				System.out.println(employees[i]);

			}

		}



	}

	

	//所有员工工资

		private void setMoney() {



			System.out.println("工号\t姓名\t总收入");

			for (int i = 0; i < employees.length; i++) {

				if (employees[i] != null) {

					System.out.println(employees[i].getId()+"\t"+employees[i].getName()+"\t"+employees[i].calMoney());

				}

			}



		}

	



	//打印菜单

	private void printMenu() {

		System.out.println("1. 添加员工");

		System.out.println("2. 删除员工");

		System.out.println("3. 修改员工");

		System.out.println("4. 列表员工");

		System.out.println("5. 让所有员工工作");

		System.out.println("6. 公司团建,让会开车的员开车");

		System.out.println("7. 临时司机开车");
		
		System.out.println("8. 打印全部员工工资");

		System.out.println("9. 退出");

		



	}

}

