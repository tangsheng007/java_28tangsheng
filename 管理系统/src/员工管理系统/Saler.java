package 员工管理系统;

public class Saler extends Employee implements IDriverAble {

	// 业绩

	double performance;

	

	// 提成比率

	double rate;

	

	public double getPerformance() {

		

		return performance;

	}



	public void setPerformance(double performance) {

		

		this.performance = performance;

	}



      

	

	// 重写work()方法

	@Override

	public void work() {

		// 随机生成业绩

		double performance = Math.random() * 100000;

		System.out.println("工号为:" + this.id + "的" + this.profession + this.name + "在销售产品," + "业绩为:" + performance + "元");

		this.performance += performance; 

	}



	@Override

	public String toString() {

		return "" + id + "\t" + name + "\t" + profession + "\t" + salary + "\t" + bouns + "\t" + performance + "\t"

				+ rate + "\t" + calMoney();

	}



	// 重写计算工资方法

	@Override

	public double calMoney() {

		

		return super.calMoney() + this.performance * this.rate;

	}



	@Override

	public void dirve() {

		// TODO Auto-generated method stub

		System.out.println(this.name + "：开车出去团建");

	}



	public Saler() {

		super();

		// TODO Auto-generated constructor stub

	}



	public Saler(int id, String name, int age, int workAge, String profession, double salary, double bouns,

			double performance, double rate) {

		super(id, name, age, workAge, "销售", salary, bouns);

		this.performance = performance;

		this.rate = rate;

	}



}

