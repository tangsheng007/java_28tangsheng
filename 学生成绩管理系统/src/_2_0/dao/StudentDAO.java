package _2_0.dao;

import java.sql.*;

import _2_0.pojo.Student;
import _2_0.utils.JDBCUtils;

public class StudentDAO {

	public void insert (Student s) throws SQLException {
	String sql="insert into student values(?,?,?,?,?)";
	// 1. 得到连接
	Connection conn = JDBCUtils.getConnection();
	// 2. 创建查询对象
	PreparedStatement stmt = conn.prepareStatement(sql);
	stmt.setInt(1, s.getId());
	stmt.setString(2, s.getName());
	stmt.setInt(3,s.getChinese());
	stmt.setInt(4, s.getMath());
	stmt.setInt(5, s.getEnglish());
	// 3. 执行
	int result = stmt.executeUpdate();
	System.out.println("影响行数:"+result);
	
	//4.释放资源
	JDBCUtils.destory(conn, stmt, null);
		
	};

}
