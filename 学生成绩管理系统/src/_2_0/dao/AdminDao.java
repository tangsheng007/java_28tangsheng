package _2_0.dao;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import _2_0.pojo.Admin;
import _2_0.pojo.Classes;
import _2_0.utils.JDBCUtils;

public class AdminDao {
	
	public void insert (Admin a) throws SQLException {
		String sql="insert into admin values(?,?,?)";
		// 1. 得到连接
		Connection conn = JDBCUtils.getConnection();
		// 2. 创建查询对象
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setInt(1, a.getId());
		stmt.setString(2, a.getUsername());
		stmt.setString(3, a.getPassword());
		
		// 3. 执行
		int result = stmt.executeUpdate();
		System.out.println("影响行数:"+result);
		// 4. 释放资源
		JDBCUtils.destory(conn, stmt, null);
		}
		public void deleteById(int id) throws SQLException{
			String sql = "delete from admin where id = ?";
			// 1. 得到连接
			Connection conn = JDBCUtils.getConnection();
			// 2. 创建查询对象
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setInt(1,id);
			// 3. 执行
			int result = stmt.executeUpdate();
			System.out.println("影响行数:"+result);
			// 4. 释放资源
			JDBCUtils.destory(conn, stmt, null);
		};
		
		// 修改
		public void update(Admin a) throws SQLException{
			
			String sql = "update admin set password = ?,username = ? where id = ?";
			// 1. 得到连接
			Connection conn = JDBCUtils.getConnection();
			// 2. 创建查询对象
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setString(1, a.getPassword());
			stmt.setString(2, a.getUsername());
			stmt.setInt(3, a.getId());
			
			// 3. 执行
			int result = stmt.executeUpdate();
			System.out.println("影响行数:"+result);
			// 4. 释放资源
			JDBCUtils.destory(conn, stmt, null);
		};
		
		// 查询
		public Admin findById(int id) throws SQLException{
			String sql = "select * from admin where id = ?";
			// 1. 得到连接
			Connection conn = JDBCUtils.getConnection();
			// 2. 创建查询对象
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setInt(1, id);
			// 3. 执行
			ResultSet rs = stmt.executeQuery();
			Admin a = null;
			if(rs.next())
			{
				String username = rs.getString(2);
				String password = rs.getString(3);
				a = new Admin(username,id,password);
			}
			
			// 4. 释放资源
			JDBCUtils.destory(conn, stmt, null);
			
			return a;
		}
		
		// 查询所有
		public List<Admin> findAll() throws SQLException{
			
			String sql = "select * from admin";
			// 1. 得到连接
			Connection conn = JDBCUtils.getConnection();
			// 2. 创建查询对象
			PreparedStatement stmt = conn.prepareStatement(sql);
			// 3. 执行
			ResultSet rs = stmt.executeQuery();
			List<Admin> list = new ArrayList<>();
			while(rs.next())
			{
				int id = rs.getInt(1);
				String username = rs.getString(2);
				String password = rs.getString(3);
				Admin a = new Admin(username,id,password);
				list.add(a);
			}
			
			// 4. 释放资源
			JDBCUtils.destory(conn, stmt, null);
			
			return list;
			
		};
}		
	
