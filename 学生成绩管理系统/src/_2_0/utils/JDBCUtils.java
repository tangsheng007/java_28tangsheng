package _2_0.utils;

import java.sql.*;

public class JDBCUtils {
	
	//1.连接数据库
		public static final String URL = "jdbc:mysql://localhost:3306/school";
		public static final String USERNAME = "root";
		public static final String PASSWORD = "123456";
		public static final String DRIVER_NAME="com.mysql.cj.jdbc.Driver";
		//0.加载驱动
		static 
		{
			try {
				Class.forName("com.mysql.cj.jdbc.Driver");
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		//1.连接数据库
		public static Connection getConnection() throws SQLException
		{
			Connection conn=DriverManager.getConnection(URL,USERNAME,PASSWORD);
			return conn;
			
		}
		
		// 2. 释放资源
			public static void destory(Connection conn,Statement stmt,ResultSet rs)
			{
				if(rs!=null)
				{
					try {
						rs.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				if(stmt!=null)
				{
					try {
						stmt.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				if(conn!=null)
				{
					try {
						conn.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}

	}
