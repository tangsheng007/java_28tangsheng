package _2_0.pojo;

public class Admin {
	
	private String username;
	private int id;
	private String password;
	
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	
	
	public Admin(String username, int id, String password) {
		super();
		this.username = username;
		this.id = id;
		this.password = password;
	}
	
	
	
	
	public Admin() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "Admin [username=" + username + ", id=" + id + ", password=" + password + "]";
	}
	
	

}
