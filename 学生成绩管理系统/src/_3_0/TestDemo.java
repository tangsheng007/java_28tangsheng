package _3_0;

import java.sql.SQLException;
import java.util.List;

import _2_0.dao.AdminDao;
import _2_0.dao.ClassDAO;
import _2_0.pojo.Admin;
import _2_0.pojo.Classes;

public class TestDemo {

	public static void main(String[] args) throws SQLException {
		
		// 班级表 classes
				// 班级编号 班级名字 
				// 学生表 student
				// 学号 姓名 语文 数学 外语 班级编号
				AdminDao dao1 = new AdminDao();
				//ClassesDAO dao = new ClassesDAO();
				// 1. 测试添加班级
			//	Classes c1 = new Classes(1,"高三1班");
				Admin a1 = new Admin("ning",02,"500");
			//	dao.insert(c1);
				dao1.insert(a1);
				
				// 2.
//				Classes c1 = new Classes(1,"高一1班");
//				dao.update(c1);
//				dao1.update(a1);
				
				// 3
//				dao1.deleteById(2);
				
				// 4.
//				Classes c = dao.findById(2);
//				Admin a = dao1.findById(1);
//				System.out.println(a);
//				System.out.println(c);

				// 5.
//				List<Classes> list = dao.findAll();
//				for(Classes c:list)
//				{
//					System.out.println(c);
//				}
				
				List<Admin> list = dao1.findAll();
				for(Admin a2:list)
				{
					System.out.println(a2);
				}

			}

		}
