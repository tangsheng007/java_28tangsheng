package 异常;

import java.io.FileWriter;
import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;



public class TestDemo2 {

	
	static Scanner sc = new Scanner(System.in);
	
	
	public static void main(String[] args)  {
		// TODO Auto-generated method stub

		
		
		
		// 文件操作的代码
//		// 打开文件
//		FileWriter f = new FileWriter("e:\\1.txt");
//		// 写入内容
//		f.write(""+12);
//		// 关闭文件
//		f.close();

		
		
		// 1. 让用户输入2个数
		// 2. 计算相除
		// 3. 将结果存入文件
		
		
		
		// foo1();
		foo2();
		
		
		
		System.out.println("程序正常结束");

		
		
	}

	private static void foo1() 
	{
		// TODO Auto-generated method stub
		try {
			System.out.println("请输入第一个数:");
			int x = sc.nextInt(); 
			System.out.println("请输入第二个数:");
			int y = sc.nextInt();
			int result = x/y;   
			System.out.println("请输入结果保存的位置");
			String path = sc.next();
			FileWriter f = new FileWriter(path);
			f.write(x+"/"+y+"="+result);
			f.close();
		}
		catch(InputMismatchException e1)
		{
			System.out.println("请输入与正确的数字");
			
		}
		catch(ArithmeticException e2)
		{
			System.out.println("除数不能为0");
		} 
		catch (IOException e) {
			System.out.println("输入的保存位置，无法保存");
		}
		
		
	}
	
	
	private static void foo2() 
	{
		while(true)
		{
			try {
				System.out.println("请输入第一个数:");
				int x = sc.nextInt(); 
				System.out.println("请输入第二个数:");
				int y = sc.nextInt();
				int result = x/y;   
				System.out.println("请输入结果保存的位置");
				String path = sc.next();
				FileWriter f = new FileWriter(path);
				f.write(x+"/"+y+"="+result);
				f.close();
				break;
			}
			catch(InputMismatchException e1)
			{
				System.out.println("请输入与正确的数字");
				sc.next(); // 把输入的接收掉
			}
			catch(ArithmeticException e2)
			{
				System.out.println("除数不能为0");
			} 
			catch (IOException e) {
				System.out.println("输入的保存位置，无法保存");
			}
		}
	}

}