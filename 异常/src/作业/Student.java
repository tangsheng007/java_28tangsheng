package 作业;

import java.io.FileWriter;
import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Student {
	
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int[] score = new int[10];
		while(true)
		{
			try 
			{
				for(int i=0;i<3;i++)
				{
					System.out.println("请输入学生成绩");
					score[i] = sc.nextInt();
				}
				System.out.println("请输入要保存的文件地址");
				String path = sc.next();
				FileWriter f = new FileWriter(path);
				for(int j=0;j<3;j++)
				{
					f.write(score[j] + ",");
				}
				f.close();
				System.out.println("程序正常结束");
				break;
			}
			catch(InputMismatchException e1)
			{
				System.out.println("请输入正确的数字");
				sc.next();
			}
			catch(IOException e)
			{
				System.out.println("请输入正确的文件地址");
			}
			
			
		}
	}

}