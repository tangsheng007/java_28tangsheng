package 内部类;

public class TestDome {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Company c1=new Company();
		c1.name="腾讯";
		c1.bossName="马化腾";
		
		Company c2=new Company();
		c2.name="阿里巴巴";
		c2.bossName="马云";
		
		// 创建内部类对象,需要使用外部类对象来创建
		Company.Employee e1=c1.new Employee();
		e1.name="张三";
		
		Company.Employee e2=c2.new Employee();
		e2.name="张三";
		
		System.out.println(e1);
		System.out.println(e2);

	}

}
