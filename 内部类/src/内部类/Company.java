package 内部类;

public class Company {
	
	String name;
	String bossName;
	
	public class Employee{
		String name;
		
		public String toString() {
			return "我的名字叫"+name+"我是"+Company.this.name+"公司的员工";
		}
	}

}
