package 练习;

import java.util.Scanner;

public class yuan {

	public static void main(String[] args) {
		// TODO 自动生成的方法存根
		// 1.计算圆的周长
		int r = 4;
		float pi = 3.14F;
		float zc = 2 * pi * r;
		System.out.println("圆的周长是：" + zc);
		
		// 2.计算3个整数之和
		int num1 = 6;
		int num2 = 10;
		int num3 = 20;
		int sum = num1 + num2 + num3;
		System.out.println("三个数之和为："+sum);
		
		// 3.定义变量存放以下信息(选择适当的数据类型)
		short year = 2042;
		byte month = 12;
		byte day = 31;
		float bankrate = 5.4f;
		char space = 32;
		float temperature = 37.3f;
		System.out.println("现在是北京时间：" + year +"年" + month + "月" + day + "日");
		System.out.println("银行利率是：" + bankrate);
		System.out.println("空格字符：" + space);
		System.out.println("体温：" + temperature);
		
		// 4.下面几行代码有没有问题?
		// byte e = 128;				溢出了
		// short s = 32767;				没问题
		// int d = 3.3;					数据类型错了，是float d = 3.3f;
		// long l = 1231231231231323;	最后没有加L，应该是long l = 1231231231231323L;
		// float a = 2.2;				没问题
		// double b = 3;				必须是小数类型，所以，double b = 3.0；
		// double c = 3.3;				没问题
				
		// 5. 编写一个程序计算长方体体积
		// (让用户输入长方体的长宽高)
		Scanner sc=new Scanner(System.in);
		System.out.print("请输入长方体的长:");
		int l = sc.nextInt();
		System.out.print("请输入长方体的宽:");
		int w = sc.nextInt();
		System.out.print("请输入长方体的高:");
		int h = sc.nextInt();
		int area = l * w * h;
		System.out.println("长方体的面积是:" + area  + "\n");
				
				
		// * 6. 编写一个程序计算人的BMI值 (BMI公式百度一下)
		// (让用户输入体重和身高)
		System.out.print("请输入自己的体重:");
		float weight = sc.nextFloat();
		System.out.print("请输入自己的身高:");
		float height = sc.nextFloat();
		float BMI = weight / (height * height);
		System.out.println("您的BMI值是:" + BMI);		

	}

}
