package 连接数据库;

import java.sql.*;

public class TestDemo {

	public static void main(String[] args) throws SQLException {
		// TODO Auto-generated method stub
		//加载驱动
//		DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver());
//		
//		System.out.println("2.连接数据库");
//		
//		String url="jdbc:mysql://localhost:3306/world";
//		
//		String username="root";
//		String password="123456";
//		
//		//开始测试
//		System.out.println("3.开始测试");
//		Connection conn=DriverManager.getConnection(url,username,password);
//		System.out.println("连接成功"+conn);
//		
//		// 创建一个数据表 student (id ,name,age)
//		String sql = "CREATE TABLE student2 (id INT PRIMARY KEY AUTO_INCREMENT,NAME VARCHAR(10),age INT);";
//		// 增删改查
//		
//		// 创建一个执行SQL语句的查询对象
//		Statement stmt = conn.createStatement();
//		// 执行sql
//		int result = stmt.executeUpdate(sql);
//		System.out.println("影响的行数:"+result);
//		
//		System.out.println("4.释放资源，关闭连接");
//		stmt.close();
//		conn.close();
//		
		//INSERT INTO student VALUES (1,'zhangsan',18)
		//CREATE TABLE student1(id INT PRIMARY KEY AUTO_INCREMENT ,NAME VARCHAR(10),age INT)
		// 1. 加载驱动
				System.out.println("1. 加载");
				DriverManager.registerDriver(new com.mysql.jdbc.Driver());// 5.x 驱动
//				DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver()); // 8.x 驱动
				
				// 2. 连接数据库
				System.out.println("2. 连接数据库");
				
				// jdbc:mysql:  协议:子协议
				// localhost	ip地址
				// 3306			端口
				// world		数据库名
				
				// 本机
//				String url = "jdbc:mysql://localhost:3306/world";
//				String username = "root";
//				String password = "123456";
				
				// 老师服务器
				String url = "jdbc:mysql://10.131.8.253:3306/student";
				String username = "student";
				String password = "123456";
				Connection conn = DriverManager.getConnection(url, username, password);
				System.out.println("连接成功"+conn);		
				// 3.
				System.out.println("3. 执行SQL");
				// 创建一个执行SQL语句的查询对象
				Statement stmt = conn.createStatement();
						
				// 3.1 更新语句
				// 创建数据表语句
//				String sql = "CREATE TABLE student2 (id INT PRIMARY KEY AUTO_INCREMENT,NAME VARCHAR(10),age INT);";
				// 增删改语句
//				String sql = "insert into student values (2,'李四',19)";
//				String sql = "delete from student where id = 1";
//				String sql = "update student set age=28 where id=2";
				// 执行sql
//				int result = stmt.executeUpdate(sql);
//				System.out.println("影响的行数:"+result);
				// 3.2 查询语句
				String sql = "select * from student";
				// executeQuery() 得到一个结果集对象
				ResultSet rs = stmt.executeQuery(sql);
				// rs.next() 指向下一行结果
				while(rs.next())
				{
					// 根据列得到得到每列内容
					int id = rs.getInt(1); // 
					String name = rs.getString(2);
					int age = rs.getInt(3);
					System.out.println(id+"\t"+name+"\t"+age);
				}
				
				// 4. 关闭连接
				System.out.println("4. 释放资源，关闭连接");
				rs.close();
				stmt.close();
				conn.close();
				
				// 练习:
				// 1. 编程,用代码连接老师服务器,建一张表格学生表 student_编号名字 比如:student_01banjianwei
				//    测试 插入若干数据 学生数据
				//    测试 删除某学生数据
				//    测试 更新某学生数据
				//    测试 查询所有学生
				
				// 教师机数据库
				// 服务器: 10.131.8.253
				// 端口: 3306
				// 用户名: student
				// 密码:123456
				// 数据库名:student
				
				
				
				
			}

		}
