package 增删改查;

import java.sql.*;

public class TestDemo {

	public static void main(String[] args) throws SQLException {
		// TODO Auto-generated method stub

//		testAdd();
		
//		testDel();
//		
//		testModify();
//		
		testFind();
	}
	
	// 创建数据表语句
//	String sql = "CREATE TABLE student2 (id INT PRIMARY KEY AUTO_INCREMENT,NAME VARCHAR(10),age INT);";
	// 增删改语句
//	String sql = "insert into student values (2,'李四',19)";
//	String sql = "delete from student where id = 1";
//	String sql = "update student set age=28 where id=2";

	private static void testFind() throws SQLException 
	{
		String sql = "select * from student";
		// 1. 得到连接
		Connection conn = JDBCUtils.getConnection();
		// 2. 创建查询对象
		Statement stmt = conn.createStatement();
		// 3. 执行
		ResultSet rs = stmt.executeQuery(sql);
		while(rs.next())
		{
			// 根据列得到得到每列内容
			int id = rs.getInt(1); // 
			String name = rs.getString(2);
			int age = rs.getInt(3);
			
			System.out.println(id+"\t"+name+"\t"+age);
		}

		// 4. 释放资源
		JDBCUtils.destory(conn, stmt, rs);
	}

	private static void testModify() throws SQLException {
		// TODO Auto-generated method stub
		String sql = "update student set age=1000 where id=101";
		// 1. 得到连接
		Connection conn = JDBCUtils.getConnection();
		// 2. 创建查询对象
		Statement stmt = conn.createStatement();
		// 3. 执行
		int result = stmt.executeUpdate(sql);
		System.out.println("影响行数:"+result);
		// 4. 释放资源
		JDBCUtils.destory(conn, stmt, null);
		
	}

	private static void testDel() throws SQLException {
		// TODO Auto-generated method stub
	
		String sql = "delete from student where id = 101";
		// 1. 得到连接
		Connection conn = JDBCUtils.getConnection();
		// 2. 创建查询对象
		Statement stmt = conn.createStatement();
		// 3. 执行
		int result = stmt.executeUpdate(sql);
		System.out.println("影响行数:"+result);
		// 4. 释放资源
		JDBCUtils.destory(conn, stmt, null);
	}

	private static void testAdd() throws SQLException {
		
		String sql = "insert into student values (101,'刘备',19)";
		// 1. 得到连接
		Connection conn = JDBCUtils.getConnection();
		// 2. 创建查询对象
		Statement stmt = conn.createStatement();
		// 3. 执行
		int result = stmt.executeUpdate(sql);
		System.out.println("影响行数:"+result);
		// 4. 释放资源
		JDBCUtils.destory(conn, stmt, null);
		
	}

	
	
}