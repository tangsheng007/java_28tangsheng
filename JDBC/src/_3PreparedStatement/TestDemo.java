package _3PreparedStatement;

import java.sql.*;

public class TestDemo {

	public static void main(String[] args) throws SQLException {
		// TODO Auto-generated method stub
		
		// 使用Statement，sql语句拼接方式 会被sql注入攻击
//		test1();

		// 使用PreparedStatement,对sql进行预编译，可防止sql注入攻击
		test2();
	}
		
		private static void test2() throws SQLException{
			
			//1.得到连接
			Connection conn=JDBCUtils.getConnection();
			//创建查询对象
			String sql="select * from user where username=? and password=?";
			PreparedStatement stmt=conn.prepareStatement(sql);
			
			// 3. 执行查询
//			String username = "zhangsan";
//			String password = "123456";
			
			// sql注入共计 
			String username = "a";
			String password = "a' or '1'='1";
			
			// 填充参数
			stmt.setString(1, username);
			stmt.setString(2, password);
			
			
			ResultSet rs = stmt.executeQuery();// 注意注意，这里不要在传入sql
			if (rs.next()) {
				System.out.println("登录成功");
			} else {
				System.out.println("登录失败");
			}
			// 4. 释放资源资源
			JDBCUtils.destory(conn, stmt, rs);
		}

		private static void test1() throws SQLException {

			// 1. 得到连接
			Connection conn = JDBCUtils.getConnection();

			// 2. 创建查询对象
			Statement stmt = conn.createStatement();

			// 3. 执行查询
//			String username = "zhangsan";
//			String password = "1234567";

			// SQL注入
			String username = "a";
			String password = "a' or '1'='1";
			String sql = "select * from user where username='" + username + "' and password='" + password + "';";
			System.out.println(sql);

			ResultSet rs = stmt.executeQuery(sql);
			if (rs.next()) {
				System.out.println("登录成功");
			} else {
				System.out.println("登录失败");
			}
			// 4. 释放资源资源
			JDBCUtils.destory(conn, stmt, rs);

		}

	}