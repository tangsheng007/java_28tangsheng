package Map接口;
import java.util.*;

public class MapDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		// Map接口
		// 按照键值对存放数据
		
		// key - value
		// key 不能重复	可以是任意类型，一般是String 或者是Integer作为key
		// value 可以重复  可以是任意类型
		
		// 1. 放入
//		V put(K key, V value) 
//        将指定的值与此映射中的指定键关联（可选操作）。 
		
		Map<Integer,String> m1 = new HashMap<>();
		m1.put(1, "张三");
		m1.put(2, "李四");
		m1.put(3, "王五");
		m1.put(1, "赵六");// 覆盖掉张三
		m1.put(4, "李四");
		System.out.println(m1);
		
		// 2. 移除
//		V remove(Object key) 
//        如果存在一个键的映射关系，则将其从此映射中移除（可选操作）。 		
//		m1.remove(2);
//		System.out.println(m1);
		
		// 3. 获取
//		 V get(Object key) 
//         返回指定键所映射的值；如果此映射不包含该键的映射关系，则返回 null 		
		System.out.println(m1.get(3));
		
		
		// 4. 信息
		System.out.println(m1.size());
		Set s1 = m1.keySet();// 得到所有的key
		System.out.println(s1);
//		Cllection<V> values() 
//        返回此映射中包含的值的 Collection 视图。 		
		Collection l1 = m1.values();// 得到所有的值
		System.out.println(l1);
		
		// 5. 遍历
		// 5.1 遍历keySet()
		System.out.println("通过keySet()遍历");
		for(Integer k:m1.keySet())
		{
			System.out.println(k+"->"+m1.get(k));
		}
		// 5.2 遍历values()
		System.out.println("通过values()遍历");
		for(String v:m1.values())
		{	
			System.out.println(v);
		}
		// 5.3 遍历entrySet()
//		Set<Map.Entry<K,V>> entrySet() 
//        返回此映射中包含的映射关系的 Set 视图。 		
//		 Map.Entry   一个键值对
			// .getKey() 得到键
			// .getValue() 得到值
		System.out.println("通过entrySet()遍历");
		for(Map.Entry<Integer, String> e:m1.entrySet())
		{
			// e 一个键值对(包含了一个key，一个value)
			System.out.println(e.getKey() +"->"+e.getValue());
		}
		
		
		// 几种实现类区别(了解)
		Map<String,String> m2 = new HashMap<>(); 	   // 这个根据hashCode()存放
		Map<String,String> m3 = new LinkedHashMap<>();// 这个根据放入顺序
		Map<String,String> m4 = new TreeMap<>();      // 根据compareTo方法的返回值排序
		
		m2.put("abc", "aaaaa");
		m2.put("aaa", "aaaaa");
		m2.put("123", "aaaaa");
		m2.put("add", "aaaaa");
		System.out.println(m2);
		
		m3.put("abc", "aaaaa");
		m3.put("aaa", "aaaaa");
		m3.put("123", "aaaaa");
		m3.put("add", "aaaaa");
		System.out.println(m3);
		
		m4.put("abc", "aaaaa");
		m4.put("aaa", "aaaaa");
		m4.put("123", "aaaaa");
		m4.put("add", "aaaaa");
		System.out.println(m4);
		
		
		// 
		Map<Integer,Person> m5 = new HashMap<>();
		m5.put(1001, new Person(1001,"张三",18));
		m5.put(1002, new Person(1002,"李四",18));
		m5.put(1003, new Person(1003,"王五",18));
		System.out.println(m5.get(1003));
		
		
	}

}
