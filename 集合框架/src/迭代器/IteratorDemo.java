package 迭代器;

import java.util.*;

public class IteratorDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		// Iterator  迭代器接口		用来遍历Collection元素
//		 boolean hasNext() 
//		          如果仍有元素可以迭代，则返回 true。 
//		 E next() 
//		          返回迭代的下一个元素。 
//		 void remove() 
//		          从迭代器指向的 collection 中移除迭代器返回的最后一个元素（可选操作）。 

		
		// Iterable	 可以迭代接口 是Collection的父接口
//			Iterator<T> iterator() 
//	        返回一个在一组 T 类型的元素上进行迭代的迭代器。 
		
		Collection<Integer> c1 = new ArrayList<>();
		
		c1.add(1);
		c1.add(3);
		c1.add(5);
		c1.add(7);
		
		// [1,3,5,7]
		
		// 得到容器上的迭代器
		Iterator<Integer> itor = c1.iterator();
		
//		System.out.println(itor.hasNext());
//		System.out.println(itor.next()); // 1
//		System.out.println(itor.hasNext());
//		System.out.println(itor.next()); // 3
//		System.out.println(itor.hasNext());
//		System.out.println(itor.next()); // 5
//		System.out.println(itor.hasNext());
//		System.out.println(itor.next()); // 7
//		System.out.println(itor.hasNext());// false
//		System.out.println(itor.next()); // Exception in thread "main" java.util.NoSuchElementException
		
//		while(itor.hasNext())
//		{
//			System.out.println(itor.next());
//		}
		
		
//		System.out.println(itor.remove());
		
		while(itor.hasNext())
		{
			int tmp = itor.next();
			if(tmp==5)
			{
				itor.remove();
			}
		}
		System.out.println(c1);
		
		
		
		// 加强型for遍历集合容器
		for(Integer i:c1)
		{
			System.out.println(i);
		}
		
		
		
		
	}

}