package List接口;

import java.util.*;

public class ListDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		// List接口
		// 规定了一个采用线性存储方式的容器必须要实现的方法
		
		// 存储顺序和添加顺序一致
		// 内容可以重复
		
		// 1. 添加
//		 boolean add(E e) 
//         向列表的尾部添加指定的元素（可选操作）。 
//void add(int index, E element) 
//         在列表的指定位置插入指定元素（可选操作）。 
//boolean addAll(Collection<? extends E> c) 
//         添加指定 collection 中的所有元素到此列表的结尾，顺序是指定 collection 的迭代器返回这些元素的顺序（可选操作）。 
//boolean addAll(int index, Collection<? extends E> c) 
//         将指定 collection 中的所有元素都插入到列表中的指定位置（可选操作）。
		
		
		List<Integer> l1 = new ArrayList<>();
		l1.add(1);
		l1.add(3);
		l1.add(1);
		l1.add(5);
		l1.add(10);
		System.out.println(l1);
		l1.add(2,4);
		System.out.println(l1);
		List<Integer> l2 = new ArrayList<>();
		l2.addAll(l1);
		l2.addAll(2, l1);
		System.out.println(l2);
		

		// 2. 移除
//		 E remove(int index) 
//         移除列表中指定位置的元素（可选操作）。 
//boolean remove(Object o) 
//         从此列表中移除第一次出现的指定元素（如果存在）（可选操作）。 
//boolean removeAll(Collection<?> c) 
//         从列表中移除指定 collection 中包含的其所有元素（可选操作）。 
		l1.remove(1);// 如果传入的int  按照位置删除
		System.out.println(l1);
		
		l1.remove(Integer.valueOf(1));// 将第一个数字1Intger对象删除
		System.out.println(l1);
		
		// 3. 获取、访问
//		 E get(int index) 
//         返回列表中指定位置的元素 
		System.out.println(l1.get(1));
		
		for(int i=0;i<l1.size();i++)
		{
			System.out.println(l1.get(i));
		}
		
		// 4. 覆盖
		System.out.println(l1);
		l1.set(2, 50);
		System.out.println(l1);
		
		// 5. 查找
		l1.add(20);
		l1.add(30);
		l1.add(50);
		l1.add(60);
//		int indexOf(Object o) 
//        返回此列表中第一次出现的指定元素的索引；如果此列表不包含该元素，则返回 -1 
//		int lastIndexOf(Object o) 
//        返回此列表中最后出现的指定元素的索引；如果列表不包含此元素，则返回 -1。 
		System.out.println(l1);
		System.out.println(l1.indexOf(50));
		System.out.println(l1.lastIndexOf(50));
		System.out.println(l1.lastIndexOf(100));
		
		
		// 6. 
//		List<E> subList(int fromIndex, int toIndex) 
//        返回列表中指定的 fromIndex（包括 ）和 toIndex（不包括）之间的部分视图。 
		List<Integer> l3 = l1.subList(4, 8);
		System.out.println(l3);
	}

}
