package List接口;

import java.util.*;

public class ListDemo2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		List<Student> c1 = new ArrayList();
		
		c1.add(new Student("张三",18,"高三1班"));
		c1.add(new Student("李四",18,"高三1班"));
		c1.add(new Student("王五",18,"高三1班"));
		
		
//		c1.remove(1);
		
		// 重写equals方法
		c1.remove(new Student("李四",18,"高三1班"));
		
		System.out.println(c1);
		
	}

}
