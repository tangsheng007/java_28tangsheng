package List接口;

import java.util.Objects;

public class Student {

	// 属性
	String name;
	int age;
	String className;
	
	// 方法
	public void eat()
	{
		System.out.println(name+"在吃饭");
	}
	
	public void study()
	{
		System.out.println(name+"在学习");
	}
	
	public void sleep()
	{
		System.out.println(name+"zzzzzzz");
	}

	public Student(String name, int age, String className) {
		super();
		this.name = name;
		this.age = age;
		this.className = className;
	}

	@Override
	public String toString() {
		return "Student [name=" + name + ", age=" + age + ", className=" + className + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(name);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Student other = (Student) obj;
		return Objects.equals(name, other.name);
	}
	
	
}
