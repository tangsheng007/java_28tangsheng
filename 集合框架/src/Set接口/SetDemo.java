package Set接口;

import java.util.*;

import List接口.Student;

public class SetDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		// Set接口
		
		// HashSet类			放入新对象的时候，会调用equals方法与容器中原有的所有对象进行比较
		// LinkedHashSet类	和HashSet类似,他的存放顺序是按照放入顺序
		// TreeSet类			
		
		// HashSet类
		Set s1 = new HashSet(); 
		s1.add(1);
		s1.add(1);
		s1.add(2);
		s1.add(1);
		s1.add(1);
		System.out.println(s1);
		
		Set s2 = new HashSet(); 
		s2.add("刘备");
		s2.add("曹操");
		s2.add("关羽");
		s2.add("曹操");
		System.out.println(s2);
		
		Set s3 = new HashSet();
		s3.add(new Student("张三",18,"高三1班"));
		s3.add(new Student("李四",18,"高三1班"));
		s3.add(new Student("张三",17,"高三1班")); // 第二个张三放不进去
		System.out.println(s3);
		
		
		// 2. LinkedHashSet	按照放入顺序
		Set s4 = new LinkedHashSet(); 
		s4.add("刘备");
		s4.add("曹操");
		s4.add("关羽");
		s4.add("曹操");
		System.out.println(s4);
		
		
		// 3. TreeSet	按照内容排序
		Set s5 = new TreeSet();
		s5.add(10);
		s5.add(5);
		s5.add(15);
		s5.add(20);
		s5.add(100);
		s5.add(0);
		s5.add(35);
		s5.add(90);
		System.out.println(s5);
		
		Set s6 = new TreeSet();
		s6.add("abc");
		s6.add("a");
		s6.add("b");
		s6.add("c");
		s6.add("dd");
		s6.add("aaa");
		System.out.println(s6);
		
		// 放入TreeSet容器的自定义类需要实现Comparable接口
		// 给TreeSet提供排序的依据
		Set s7 = new TreeSet();
		// Exception in thread "main" java.lang.ClassCastException: class _05Set接口.Person cannot be cast to class java.lang.Comparable
		s7.add(new Person(1,"张三",18));
		s7.add(new Person(5,"李四",19));
		s7.add(new Person(3,"王五",17));
		System.out.println(s7);
		
		
		
	}

}
