package Set接口;

public class Person implements Comparable<Person>{
	
	int id;
	String name;
	int age;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public Person() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Person(int id, String name, int age) {
		super();
		this.id = id;
		this.name = name;
		this.age = age;
	}
	@Override
	public String toString() {
		return "Person [id=" + id + ", name=" + name + ", age=" + age + "]";
	}
	
	@Override
	public int compareTo(Person o) 
	{
		// 0 表示2个对象相等
		// 1 比传入对象大
		// -1 比传入对象小
		
		// 根据id的大小排
//		if(this.id<o.id)
//		{
//			return -1;
//		}
//		else if(this.id>o.id)
//		{
//			return 1;
//		}
		
		// 根据年龄来排序
		if(this.age<o.age)
		{
			return -1;
		}
		else if(this.age>o.getAge())
		{
			return 1;
		}
		
		
		return 0;
	}
	
	

}