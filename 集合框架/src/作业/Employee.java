package ��ҵ;

import java.util.Objects;

public class Employee
{
	int id;
	String name;
	String department;
	
	public int getId()
	{
		return id;
	}
	public void setId(int id)
	{
		this.id = id;
	}
	public String getName()
	{
		return name;
	}
	public void setName(String name)
	{
		this.name = name;
	}
	public String getDepartment()
	{
		return department;
	}
	public void setDepartment(String department)
	{
		this.department = department;
	}
	public Employee(int id, String name, String department)
	{
		super();
		this.id = id;
		this.name = name;
		this.department = department;
	}
	@Override
	public String toString()
	{
		return "Employee [id=" + id + ", name=" + name + ", department=" + department + "]";
	}
	@Override
	public int hashCode()
	{
		return Objects.hash(id);
	}
	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Employee other = (Employee) obj;
		return id == other.id;
	}
	
	
}