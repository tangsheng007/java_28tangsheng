package ��ҵ;

public class Book
{
	String id;
	String name;
	Double price;
	public String getId()
	{
		return id;
	}
	public void setId(String id)
	{
		this.id = id;
	}
	public String getName()
	{
		return name;
	}
	public void setName(String name)
	{
		this.name = name;
	}
	public Double getPrice()
	{
		return price;
	}
	public void setPrice(Double price)
	{
		this.price = price;
	}
	public Book(String id, String name, Double price)
	{
		super();
		this.id = id;
		this.name = name;
		this.price = price;
	}
	public Book()
	{
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString()
	{
		return "Book [id=" + id + ", name=" + name + ", price=" + price + "]";
	}
	
}