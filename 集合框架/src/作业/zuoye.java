package 作业;

import java.util.*;

public class zuoye {

	public static void main(String[] args) {
		
		// Collection接口
		// 定义了一个容器基本的方法
		Collection c1=new ArrayList();
		Collection c2=new HashSet();
		
		c1.add("布加迪");
		c1.add("雪佛兰");
		c1.add("奥迪");
		System.out.println(c1);
		c2.add("兰博基尼");
		c2.add("保时捷");
		c2.add("奔驰");
		System.out.println(c2);
		
		System.out.println(c1.remove("布加迪"));
		
		System.out.println("数量:"+c1.size());
		
		System.out.println(c1.contains("奥迪"));
		
		System.out.println(c1.isEmpty());
		
		c2.clear();
		System.out.println(c2.isEmpty());
		
		
		Iterator<Integer> itor=c1.iterator();
		while(itor.hasNext())
		{
			System.out.println(itor.next());
		}
		Iterator<String> itor2=c1.iterator();
		while(itor2.hasNext())
		{
			String tmp=itor2.next();
			if(tmp=="奥迪")
			{
				itor2.remove();
			}
		}
		System.out.println(c1);
		
		for(Object i:c1)
		{
			System.out.println(i);
		}
		
		
		List<Employee> c4 = new ArrayList();
		c4.add(new Employee(1001,"乔布斯","销售部"));
		c4.add(new Employee(1002,"盖茨","工程部"));
		c4.add(new Employee(1003,"赵云","客服部"));			
		// 使用迭代器 1001编号的员工
		Iterator<Employee> itor1 = c4.iterator();
		while(itor1.hasNext()) {
			Employee tmp = itor1.next();
			if(tmp.getId()==1001) {
				
				System.out.println(tmp.toString());
				
			}
			
		}
		
		List<String> l1=new ArrayList<>();
		List<String> l2=new ArrayList<>();
		l1.add("A");
		l1.add("B");
		l1.add("C");
		System.out.println(l1);
		l2.add("D");
		l2.add("E");
		l2.add("F");
		l2.add("Egg");
		l2.add("Egg");
		System.out.println(l2);
		
		l1.addAll(l2);
		System.out.println(l1);
		l1.set(2,"Z");
		System.out.println(l1);
		System.out.println(l1.size());
		System.out.println(l2.size());
		System.out.println(l1.get(1));
		
		l1.remove(1);
		System.out.println(l1);
		l1.remove("B");
		System.out.println(l1);
		
		//6.
		
		Scanner sc=new Scanner(System.in);
		Set s6 = new HashSet();
		while(true)
		{
			System.out.println("输入10个数字：");
			int m=sc.nextInt();
			s6.add(m);
			if(s6.size()==10)
			{	 
				break;
			}
		}
		System.out.println(s6);

		
		//7.
		Set s7=new HashSet();
		s7.add(new Employee2(1, "乔布斯", 1000));
		s7.add(new Employee2(2, "盖茨", 2000));
		s7.add(new Employee2(3, "赵云", 3000));
		System.out.println(s7);
		// 7.2 将3个员工放入一个TreeSet容器 (Employee需要实现Comparable接口) 
		Set s72=new TreeSet();
		s72.add(new Employee2(1, "乔布斯", 1000));
		s72.add(new Employee2(2, "盖茨", 2000));
		s72.add(new Employee2(3, "赵云", 3000));
		System.out.println(s72);
		
		//8.
		
		Map<String,String> m1=new HashMap<>();
		m1.put("ISBN-001","iOS开发从入门到窒息");
		m1.put("ISBN-002","Python从入门到出家");
		m1.put("ISBN-003","Eclipse从安装的到卸载");
		System.out.println(m1);
		
		Map<String,Book> m2=new HashMap<>();
		m2.put("ISBN-001",new Book("ISBN-001","iOS开发从入门到窒息",17.5));
		m2.put("ISBN-002",new Book("ISBN-002","Python从入门到出家",30.0));
		m2.put("ISBN-003",new Book("ISBN-003","Eclipse从安装的到卸载",45.0));
		System.out.println(m2);
	}

}
