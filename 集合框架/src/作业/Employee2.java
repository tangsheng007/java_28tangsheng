package ��ҵ;

import java.util.Objects;

public class Employee2 implements Comparable<Employee2>
{
	int id;
	String name;
	int glary;
	public int getId()
	{
		return id;
	}
	public void setId(int id)
	{
		this.id = id;
	}
	public String getName()
	{
		return name;
	}
	public void setName(String name)
	{
		this.name = name;
	}
	public int getGlary()
	{
		return glary;
	}
	public void setGlary(int glary)
	{
		this.glary = glary;
	}
	public Employee2(int id, String name, int glary)
	{
		super();
		this.id = id;
		this.name = name;
		this.glary = glary;
	}
	public Employee2()
	{
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString()
	{
		return "Employee2 [id=" + id + ", name=" + name + ", glary=" + glary + "]";
	}
	@Override
	public int hashCode()
	{
		return Objects.hash(id);
	}
	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Employee2 other = (Employee2) obj;
		return id == other.id;
	}
	@Override
	public int compareTo(Employee2 o)
	{
		if(this.id<o.id)
		{
			return -1;
		}
		else if(this.id>o.getId())
		{
			return 1;
		}
		
		return 0;
	}
	
}