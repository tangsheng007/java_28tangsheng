package 集合框架;

import java.util.*;

public class 例题 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Collection接口
		// 定义了一个容器基本的方法
		
		Collection c1 = new ArrayList();
		Collection c2 = new HashSet();
		
		
		// 1. 添加元素
//boolean add(E e) 
//        确保此 collection 包含指定的元素（可选操作）。 
//boolean addAll(Collection<? extends E> c) 
//        将指定 collection 中的所有元素都添加到此 collection 中（可选操作）。 
		c1.add(1);
		c1.add(3);
		c1.add(5);
		System.out.println(c1);
		
		c2.add("张三");
		c2.add("李四");
		c2.add("王五");
		System.out.println(c2.add("张三"));// false
		System.out.println(c2);
		
		c1.addAll(c2);
		System.out.println(c1);
		
		// 2. 移除元素
//boolean remove(Object o) 
//        从此 collection 中移除指定元素的单个实例，如果存在的话（可选操作）。 
//boolean removeAll(Collection<?> c) 
//        移除此 collection 中那些也包含在指定 collection 中的所有元素（可选操作）。 
		
		System.out.println(c1.remove("张三")); // true
		System.out.println(c1.remove("张三")); // false
		
		c1.removeAll(c2);
		System.out.println(c1);
		
		
		// 3. 
		System.out.println("数量:"+c1.size());
		
		// 4.
//		boolean contains(Object o) 
//        如果此 collection 包含指定的元素，则返回 true。 
//boolean containsAll(Collection<?> c) 
//        如果此 collection 包含指定 collection 中的所有元素，则返回 true 
		System.out.println(c2.contains("张三"));
		System.out.println(c1.containsAll(c2));
		
//		System.out.println(c1.isEmpty());// 返回是否是空的
//		c1.clear(); // 清空
//		System.out.println(c1.isEmpty());
//		System.out.println(c1);
		
		// 5. 求交集
		Collection c3 = new ArrayList(); // 1 3 5
		Collection c4 = new ArrayList(); // 	5 6 7
		c3.add(1);
		c3.add(3);
		c3.add(5);
		
		c4.add(5);
		c4.add(6);
		c4.add(7);
		
		c3.retainAll(c4);
		System.out.println(c3);
		
		// 6 转化为数组
		Object[] arr = c1.toArray();
	}

}
