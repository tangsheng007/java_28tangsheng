package �γ�;

public class Student {

	private String name;
	private int id;
	private int age;
	private int galde;
	private int height;
	
	
	
	public Student() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Student(String name, int id, int age, int galde, int height) {
		super();
		this.name = name;
		this.id = id;
		this.age = age;
		this.galde = galde;
		this.height = height;
	}
	public String getName()
	{
		return name;
	}
	public void  setName(String name) 
	{
		this.name=name;
	}
	
	public int getId()
	{
		return id;
	}
	public void setId(int id) 
	{
		this.id=id;
	}
	public int getAge() 
	{
		return age;
	}
	public void setAge(int age)
	{
		this.age=age;
	}
	public int getGalde() 
	{
		return galde;
	}
	public void setGalde(int galde)
	{
		this.galde=galde;
	}
	
	public int getHeight()
	{
		return height;
	}
	public void setHeight(int height) {
		this.height=height;
		
	}
	
	@Override
	public String toString() {
		return "Student [name=" + name + ", id=" + id + ", age=" + age + ", galde=" + galde + ", height=" + height
				+ "]";
	}
	
	

}