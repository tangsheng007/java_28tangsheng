package _0930练习;

public class TestDemo {

	public static void main(String[] args) {

		
		// 分数
		Fraction f1 = new Fraction();
		f1.setN(1);
		f1.setD(2);
		
		Fraction f2 = new Fraction(1,3);
		
		System.out.println(f1);
		System.out.println(f2);
		
		// + 
		// 算术运算 字符串连接
		
//		Fraction f3 = f1 + f2; 对象无法使用+号进行运算
		// f1 f2 引用类型的变量 存放的是内存地址
		
		// 对象间的操作，只能通过方法的传递
		
		Fraction f3 = f1.add(f2);
		System.out.println(f3);
		
	}

}