package _0930练习;

public class Fraction {

	
	private int n;
	private int d;
	
	public Fraction(int n, int d) {
		super();
		this.n = n;
		this.d = d;
	}
	public Fraction() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public int getN() {
		return n;
	}
	public void setN(int n) {
		this.n = n;
	}
	public int getD() {
		return d;
	}
	public void setD(int d) {
		this.d = d;
	}
	
	@Override
	public String toString() {
		return n+"/"+d;
	}
	
	
	
	
	// 方式1
	public Fraction add(Fraction o) // o 传入另外一个分数
	{
		// this  表示当前执行的对象  (f1) 
		// o     传入的对象		(f2)
		
		// 根据公式计算 结果分子和分母
		// a/b + c/d =   (a*d+b*c)/(b*d)
		int tmpN =  this.n * o.d + this.d * o.n;
		int tmpD = this.d * o.d;
		
		// 放入一个新的分数对象，返回出去
		Fraction result = new Fraction();
		result.n = tmpN;
		result.d = tmpD;
		
		return result;
	}
		
}