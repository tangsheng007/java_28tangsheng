package _0930练习;

public class Face {

	String type;// 
	
	// 子对象
	Eye eye;
	
	public Face()
	{
		type = "国字";
		eye = new Eye();
	}
	
	@Override
	public String toString() {
		return "一张"+type+"脸，有"+eye;
	}
	
}