package 继承练习2;

public class Cat extends Animal{
	
	// 子类重新定义了sing()方法,覆盖了继承自父类的sing()
	// @Override 注解(检查这个方法是否是重写了父类的方法)
	@Override
	public void sing() {
		// TODO Auto-generated method stub
		System.out.println(name + "喵喵喵~~~");
	}
	
	public void catchMouse()
	{
		System.out.println(name + "抓老鼠");
	}

}
