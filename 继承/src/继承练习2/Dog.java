package 继承练习2;

public class Dog extends Animal{
	
	// 1. 属性
	
	// 变量定义在类的内部,这种变量称之为成员变量
	// 作用范围是整个类的内部
	// 名字

	
	
	// 2. 方法	
	public void watchDoor()
	{
		System.out.println(name + "看门");
	}

	// 使用System.out.println(d1) 打印对象的时候,输出的内容
	@Override
	public String toString() {
		return "Dog [name=" + name + ", age=" + age + "]";
	}

	public void pee() {
		System.out.println("撒了泡尿~~~");
	}
	
	@Override
	public void sing() {
		System.out.println("汪汪汪~~~");
	}
	
	
	
}