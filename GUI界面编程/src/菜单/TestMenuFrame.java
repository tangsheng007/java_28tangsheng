package 菜单;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;


public class TestMenuFrame extends JFrame implements ActionListener{
	
	
	public TestMenuFrame()
	{
		
		this.setVisible(true);
		this.setBounds(0, 0, 800, 600);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JMenuBar bar = new JMenuBar();
		this.setJMenuBar(bar);
		
		JMenu m1 = new JMenu("文件");
		JMenu m2 = new JMenu("帮助");
		bar.add(m1);
		bar.add(m2);
		
		JMenuItem m1_1 = new JMenuItem("新建");
		JMenuItem m1_2 = new JMenuItem("打开");
		JMenuItem m1_3 = new JMenuItem("关闭");
		JMenuItem m1_4 = new JMenuItem("退出");
		m1.add(m1_1);
		m1.add(m1_2);
		m1.add(m1_3);
		m1.add(m1_4);
		
		m1_1.addActionListener(this);
		m1_2.addActionListener(this);
		m1_3.addActionListener(this);
		m1_4.addActionListener(this);
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		switch(e.getActionCommand())
		{
		case "新建":
			break;
		case "打开":
			break;
		case "关闭":
			break;
		case "退出":
			System.exit(0);
			break;
		}
		
	}
	

}