package 布局;

import java.awt.BorderLayout;

import javax.swing.*;

public class BorderLayoutFrame extends JFrame {
	public BorderLayoutFrame()
	{
		this.setVisible(true);
		this.setBounds(0, 0, 800, 600);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JButton btn1 = new JButton("按钮1");
		JButton btn2 = new JButton("按钮2");
		JButton btn3 = new JButton("按钮3");
		JButton btn4 = new JButton("按钮4");
		JButton btn5 = new JButton("按钮5");
		
		this.add(btn1,BorderLayout.NORTH);
		this.add(btn2,BorderLayout.SOUTH);
		this.add(btn3,BorderLayout.WEST);
		this.add(btn4,BorderLayout.EAST);
		this.add(btn5,BorderLayout.CENTER);
		
		
		
	}
	

}
