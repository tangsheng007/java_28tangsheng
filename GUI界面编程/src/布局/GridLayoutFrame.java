package ����;

import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class GridLayoutFrame extends JFrame{
	
	public GridLayoutFrame()
	{
		this.setVisible(true);
		this.setBounds(0, 0, 800, 600);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		
		GridLayout gl = new GridLayout(10,10,5,5);
		this.setLayout(gl);
		
		for(int i=0;i<10;i++)
		{
			for(int j=0;j<10;j++)
			{
				JPanel p = new JPanel();
				int r = (int)(Math.random()*256);
				int g = (int)(Math.random()*256);
				int b = (int)(Math.random()*256);
				Color c = new Color(r,g,b);
				p.setBackground(c);
				this.add(p);
			}
		}
		
		
	}

}