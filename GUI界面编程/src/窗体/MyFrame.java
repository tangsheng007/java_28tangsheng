package 窗体;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class MyFrame extends JFrame 
{
	public MyFrame()
	{
		this.setVisible(true);
		
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		this.setLayout(null);
		
		JLabel l1=new JLabel("+");
		
		this.add(l1);
		
		l1.setBounds(130,50,100,40);
		JTextField f1 = new JTextField();
		this.add(f1);
		f1.setBounds(50, 50, 80, 40);
		
		JTextField f2 = new JTextField();
		this.add(f2);
		f2.setBounds(150, 50, 80, 40);
		
		JButton btn = new JButton("=");
		this.add(btn);
		btn.setBounds(230, 50, 80, 40);
		
		JLabel l2 = new JLabel("?");
		// 放到窗体上
		this.add(l2);
		// 设置位置及宽高
		l2.setBounds(320, 50, 100, 40);
		
		
		// 给按钮添加一个事件监听对象
		btn.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// 计算
				int num1 = Integer.parseInt(f1.getText());
				int num2 = Integer.parseInt(f2.getText());
				int result = num1+num2;
				// 
				l2.setText(""+result);
				
			}
		});
		
	}
}