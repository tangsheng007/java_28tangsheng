package 组件;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class RegisterFrame extends JFrame{
	
	
	public RegisterFrame()
	{
		this.setVisible(true);
		this.setBounds(0, 0, 800, 600);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		// 把默认的界面布局设置为null,采用绝对布局
		this.setLayout(null);
		
		JLabel l1 = new JLabel("用户名:");
		this.add(l1);
		l1.setBounds(0,30,80,40);	
		
		JLabel l2 = new JLabel("密  码:");
		this.add(l2);
		l2.setBounds(0,80,80,40);
		
		// 输入用户名
		JTextField username = new JTextField();
		this.add(username);
		username.setBounds(100,30,80,40);
		// 密码框
		JPasswordField password = new JPasswordField();
		this.add(password);
		password.setBounds(100,80, 80, 40);
		
		// 下拉框 ？？？
		JComboBox<String> cmb = new JComboBox<>();
		cmb.addItem("请选择你的性别");
		cmb.addItem("男");
		cmb.addItem("女");
		cmb.setBounds(100,150,300,30);
		this.add(cmb);
		
		// 滑动条
		JSlider s = new JSlider(18,100,18);
		this.add(s);
		s.setBounds(100,190,100,20);
		
		// 文本输入框
		JTextArea area = new JTextArea();
		JScrollPane sp = new JScrollPane(area);
		this.add(sp);
		sp.setBounds(100,220,300,80);
		
		
		
		// 登录按钮
//		JButton btn = new JButton("注册");
//		this.add(btn);
//		btn.setBounds(100, 250, 80, 40);
//		
//		// 给按钮的事件监听
//		btn.addActionListener(new ActionListener() {
//			
//			@Override
//			public void actionPerformed(ActionEvent e) {
//				String n = username.getText();
//				// getPassword() 返回 char[]
//				String p = new String(password.getPassword());
//				
//				if(n.equals("admin") && p.equals("123"))
//				{
//					// 消息框
//					JOptionPane.showMessageDialog(null, "登录成功");
//				}
//				else
//				{
//					JOptionPane.showMessageDialog(null, "登录失败");
//				}
//				
//			}
//		});
	}

}