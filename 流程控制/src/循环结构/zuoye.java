package 循环结构;

import java.util.Scanner;

public class zuoye {

	public static void main(String[] args) {
		// TODO 自动生成的方法存根
		// 1.
		int a=1;
		while(a<=100){
			System.out.println("*");
			a++;
		}
		//2.
		Scanner sc=new Scanner(System.in);
		System.out.println("输入需要打印的数量");
		int n=sc.nextInt();
		int i=1;
		while(i<=n){
			System.out.println("*");
			i++;
		}
		
		//3.
		int b =100;
		while(b>=0){
			System.out.println(b);
			b--;
		}
		//4.
		int c=1;
		int sum=1;
		while(c<=10){
			sum=sum*c;
			c++;
		}
		System.out.println(sum);
		//5.
		System.out.println("输入一个数字:");
		int m = sc.nextInt();
		int z = 1;
		int x;
		while (z <= m)
		{
			x = z * z;
			System.out.println(x);
			z++;
		
		}
		// 6. 用户输入n,打印 1,2,4,8,16,32,65,...,2^n
		// System.out.println((int)Math.pow(2, 10));
		Scanner sc1 = new Scanner(System.in);
		System.out.println("请输入n：");
		int n6 = sc1.nextInt();
		for (int l = 0; l < n6; l++)
		{
			System.out.println((int) Math.pow(2, l));
		}

		// 7. 打印-10.0,-9.9,-9.8,...,9.9,10.0
		for (double i7 = -10.0; i7 > 10.0; i7 += 0.1)
			{
				System.out.println(i7);
			}
			// 8. 打印2 4 6 8 10 ... 100
			for (int i8 = 2; i8 < 101; i8 += 2)
			{
				System.out.println(i8);
			}

			// *9. 打印20位菲波那切数列 (提示额外定义2个变量保存前两位数,用于计算下一位)
			// 1 1 2 3 5 8 13 21 34 55 ...
			int ii = 1;
			int xx = 1;
			int sum1 = ii + xx;
			System.out.println(ii + " ");
			System.out.println(xx + " ");
			for (int i9 = 1; i9 < 18; i9++)
			{
				System.out.println(sum1 + ",");
				ii = xx;
				xx = sum1;
				sum1 = ii + xx;
			}

			// 10. 遍历判断所有的三位数是否是水仙花数，结合之前的判断语句
			// (遍历: 逐个访问、逐个判断的意思)
			for (int i0 = 100; i0 <= 999; i0++)
			{
				int c1 = i0 % 10;
				int c2 = i0 / 10 % 10;
				int c3 = i0 / 100;
				if (c1 * c1 * c1 + c2 * c2 * c2 + c3 * c3 * c3 == i0)
					System.out.println(i0);
			}
				// 11
			int tz = (int) (Math.random() * 6) + 1;
			int sum2 = 0;
			for (int y = 0; y <= 20; y++)
			{
				sum2 = sum2 + tz;
			}
			System.out.println(sum2);

			// 12
			int sum3 = 0;
			for (int j = 0; j <= 20; j++)
				{
					if (tz == 6)
						break;
					sum2 = sum2 + tz;
				}
				System.out.println(sum2);

				// 13
				int sum11 = 0;
				for (int k = 0; k <= 20; k++)
				{
					if (tz == 4)
						continue;
					sum11 = sum11 + tz;
				}
				System.out.println(sum11);
		

	}

}
