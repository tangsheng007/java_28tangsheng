package 流程控制;

import java.util.Scanner;

public class ifExercise {

	public static void main(String[] args) {
		// TODO 自动生成的方法存根
		//1.用户输入2个数判断输出较大的
		Scanner sc=new Scanner(System.in);
		System.out.println("输入两个数：");
		int i=sc.nextInt();
		int j=sc.nextInt();
		
		if(i>j){
			System.out.println("较大的数为："+i);
		}
		else
			System.out.println("较大的数为："+j);
		
		//2.用户输入1个数判断是否被3整除
		System.out.println("请输入一个数:");
		int n1 = sc.nextInt();
		if(n1%3 == 0)
		{
			System.out.println("能被三整除");
		}
		else
		{
			System.out.println("不能被三整除");
		}
		
		
		// 3. 用户输入一个数字(1-7),输出中文的星期几
		System.out.println("请输入(1-7)中的一个数:");
		int n2 = sc.nextInt();
		if(n2 == 1)
		{
			System.out.println("今天是星期一");
		}
		else if(n2 == 2)
		{
			System.out.println("今天是星期二");
		}
		else if(n2 == 3)
		{
			System.out.println("今天是星期三");
		}
		else if(n2 == 4)
		{
			System.out.println("今天是星期四");
		}
		else if(n2 == 5)
		{
			System.out.println("今天是星期五");
		}
		else if(n2 == 6)
		{
			System.out.println("今天是星期六");
		}
		else
		{
			System.out.println("今天是星期日");
		}
		
		// 4. 用户输入一个数字(1-7),输出工作日还是休息日
		System.out.println("请输入(1-7)中的一个数:");
		int n3 = sc.nextInt();
		if(n3>=1&&n3<=5)
		{
			System.out.println("今天是工作日");
		}
		else
		{
			System.out.println("今天是休息日");
		}
		System.out.println();
		
		
		// 5. 输入小明的数学成绩,输出成绩等级
		System.out.println("请输入小明的数学成绩:");
		int score = sc.nextInt();
		if(score<=100&&score>=90)
		{
			System.out.println("优秀");
		}
		else if(score>=80)
		{
			System.out.println("良好");
		}
		else if(score>=70)
		{
			System.out.println("中等");
		}
		else if(score>=60)
		{
			System.out.println("及格");
		}
		else
		{
			System.out.println("不及格");
		}
		
		
		// 6. 用户输入一个年份,判断是否是闰年
		System.out.println("请输入一个年份:");
		int year = sc.nextInt();
		if((year%400==0)||(year%100!=0&&year%4==0))
		{
			System.out.println(year + "是闰年");
		}
		else
		{
			System.out.println(year + "不是闰年");
		}
		
		// 7. 让用户输入一个三位数,判断是否是水仙花数
		System.out.println("请输入一个三位数:");
		int n4 = sc.nextInt();
		int s1 = n4/100;
		int s2 = n4/10%10;
		int s3 = n4%10;
		int sum = s1*s1*s1 + s2*s2*s2 + s3*s3*s3;
		if(n4 == sum)
		{
			System.out.println("是水仙花数");
		}
		else
		{
			System.out.println("不是水仙花数");
		}

	}

}
