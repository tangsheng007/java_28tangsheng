package _0708;

public class FloatDemo {

	public static void main(String[] args) {
		// TODO 自动生成的方法存根
		
		//float		4		32		6~7
		//double	8		64		15~16
		
		float a=3.141592612312312312123F;
		double b=3.1415926123213213231312F;
		
		System.out.println(a);
		System.out.println(b);
		
		
		int r = 5;
		double area = Math.PI * r * r;
		
		System.out.println(area);

	}

}
