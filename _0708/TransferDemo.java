package _0708;

public class TransferDemo {

	public static void main(String[] args) {
		// TODO 自动生成的方法存根
		
		//自动转换
//		byte a=15;
//		short b=a;
//		int c=b;
//		long d=c;
//		float e=d;
//		double f=e;
		
		//强制转换
//		double a=3.141592;
//		float b=(float)a;
//		long c=(long)b;
//		int d=(int)c;
//		short e=(short)d;
//		byte f=(byte)e;
//		
//		System.out.println(f);
		
		//大数据类型放入小数据类型的变量  溢出
		short c1=0b00000001_10000000;
		System.out.println(c1);
		byte c2=(byte)c1;//0b10000000
		System.out.println(c2);
		
		

	}

}
