package _0708;

public class MathOperatorDemo {

	public static void main(String[] args) {
		// TODO 自动生成的方法存根
		
		int a=39;
		int b=20;
		
		int c;
		c=a+b;
		System.out.println("a+b="+c);
		c=a-b;
		System.out.println("a-b="+c);
		c=a*b;
		System.out.println("a*b="+c);
		c=a/b;
		System.out.println("a/b="+c);
		c=a%b;
		System.out.println("a%b="+c);
		
		float f1=11.0F;
		float f2=3.5F;
		System.out.println(f1+f2);
		System.out.println(f1*f2);
		System.out.println(f1/f2);
		System.out.println(f1-f2);
		System.out.println(f1%f2);
		
		//多种数据类型运算		表达式的值是最大的那个类型
		int r=5;
		float pi=3.1415926F;
		float area=pi*r*r;
		
		//byte short char 如果进行算术运算，都是转为int进行运算
		
		byte s1=5;
		byte s2=10;
		byte s3;
		s3=(byte)(s1+s2);
		System.out.println(s3);
		
		char cc='a';
		char dd=(char)(cc+1);
		System.out.println(dd);
		
		//++--
		//单独使用
		//在原来的基础上+1
		int i=10;
		i++;
		System.out.println(i);
		++i;
		System.out.println(i);
		
		//++i	先自增	后用于表达式
		//i++	先用于表达式计算		自增
		int j=10;
//		int x=++j;
//		System.out.println("j="+j+",x="+x);//j=11,x=11;
//		
		int x=j++;
		System.out.println("j="+j+",x="+x);

	}

}
