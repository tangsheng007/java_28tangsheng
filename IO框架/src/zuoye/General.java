package zuoye;

import java.io.Serializable;

public class General implements Serializable {
	
	int id;
	String name;
	String power;
	int IQ;
	int force;
	int commander;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPower() {
		return power;
	}
	public void setPower(String power) {
		this.power = power;
	}
	public int getIQ() {
		return IQ;
	}
	public void setIQ(int iQ) {
		IQ = iQ;
	}
	public int getForce() {
		return force;
	}
	public void setForce(int force) {
		this.force = force;
	}
	public int getCommander() {
		return commander;
	}
	public void setCommander(int commander) {
		this.commander = commander;
	}
	public General(int id, String name, String power, int iQ, int force, int commander) {
		super();
		this.id = id;
		this.name = name;
		this.power = power;
		IQ = iQ;
		this.force = force;
		this.commander = commander;
	}
	public General() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "General [id=" + id + ", name=" + name + ", power=" + power + ", IQ=" + IQ + ", force=" + force
				+ ", commander=" + commander + "]";
	}
	
	
	
}