package zuoye;

import java.io.*;
import java.util.Properties;
import java.util.Scanner;

public class Work {

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		// TODO Auto-generated method stub
		
		// [File 类 练习]
		// 1. 使用代码实现以下功能
		// * 在d盘创建一个目录abc
		// * 在abc下创建一个目录bbb
		// * 在abc下用记事本创建一个文件test.txt,内容"Hello world!"
		// * 将test.txt重命名为test2.txt
		// * 得到test2.txt文件的长度信息
		// * 在bbb下创建一个文件hello.txt
		// * 移动hello.txt到abc下
		// * 删除test2.txt

		// * 2.
		// 实现类似DOS命令tree的功能
		// 像是某个目录下所有的文件及文件层次信息
		// (可采用递归实现)
		// 如下效果:
		// d:\abc
		// |- aaa
		// |- bbb
		// |- hello.txt
		// |- ccc
		// |- ddd
		// |-- 1.txt
		// |- a1.txt
		// |- a2.txt

		// 如果做不出来
		// 可先实现显示e:\abc下内容，不显示子目录的内容
		// d:\abc
		// |- aaa
		// |- bbb
		// |- ccc
		// |- a1.txt
		// |- a2.txt

//		tree("e:/abc", 1);

		
		// 二 字符流练习
		// 3. 将以下内容使用字符输出流写入到文件
		// 再使用字符输入流读取出来

		// 编号,姓名,语文,数学,外语
		// 1,张三,89,100,97
		// 2,李四,95,75,80
		// 3,王五,60,100,97

		// * 4. 编写一个copy(String srcPath,String destPath)实现文件复制
//				copy("d:/1.txt","d:/2.txt");
		// 思路:
		// a 打开输入流准备读取1.txt
		// 打开输出流准备写入2.txt
		// b 循环从输入流读取内容
		// 将读取到的内容写入到2.txt
		// c. 关闭两个流
		
		
		// 三 字节流练习
		// 5.  
		// 使用字节输入流读取图片的二进制数据
		
		// 6.
		// 使用字节输出流将以下内容写入到1个文件
//		byte[] buf= {97,98,99,100};
		
		// * 7.
		// 编写一个copy(String srcPath,String destPath)实现复制文件
		
		
		// 四、高效流
		// 8. 测试高效流复制一个视频文件，对比一下使用和不使用的差别
		
		
		// 9. 将以下内容使用数据流输出流写入到文件,再读出
		// 编号,姓名,语文,数学,外语
		// 1 张三 89 100 97
		// 2 李四 95 75 80
		// 3 王五 60 100 97
		
//		DataOutputStream out = new DataOutputStream(new FileOutputStream("e:/1.data"));
//		DataInputStream in = new DataInputStream(new FileInputStream("e:/1.data"));
//		out.writeInt(1);
//		out.writeUTF(" ");
//		out.writeUTF("张三");
//		out.writeUTF(" ");
//		out.writeInt(89);
//		out.writeUTF(" ");
//		out.writeInt(100);
//		out.writeUTF(" ");
//		out.writeInt(97);
//		out.writeChar('\n');
//		
//		out.writeInt(2);
//		out.writeUTF(" ");
//		out.writeUTF("李四");
//		out.writeUTF(" ");
//		out.writeInt(95);
//		out.writeUTF(" ");
//		out.writeInt(75);
//		out.writeUTF(" ");
//		out.writeInt(80);
//		out.writeChar('\n');
//		
//		out.writeInt(3);
//		out.writeUTF(" ");
//		out.writeUTF("王五");
//		out.writeUTF(" ");
//		out.writeInt(60);
//		out.writeUTF(" ");
//		out.writeInt(100);
//		out.writeUTF(" ");
//		out.writeInt(97);
//		
//		System.out.print(in.readInt());
//		System.out.print(in.readUTF());
//		System.out.print(in.readUTF());
//		System.out.print(in.readUTF());
//		System.out.print(in.readInt());
//		System.out.print(in.readUTF());
//		System.out.print(in.readInt());
//		System.out.print(in.readUTF());
//		System.out.print(in.readInt());
//		System.out.print(in.readChar());
//		
//		System.out.print(in.readInt());
//		System.out.print(in.readUTF());
//		System.out.print(in.readUTF());
//		System.out.print(in.readUTF());
//		System.out.print(in.readInt());
//		System.out.print(in.readUTF());
//		System.out.print(in.readInt());
//		System.out.print(in.readUTF());
//		System.out.print(in.readInt());
//		System.out.print(in.readChar());
//		
//		System.out.print(in.readInt());
//		System.out.print(in.readUTF());
//		System.out.print(in.readUTF());
//		System.out.print(in.readUTF());
//		System.out.print(in.readInt());
//		System.out.print(in.readUTF());
//		System.out.print(in.readInt());
//		System.out.print(in.readUTF());
//		System.out.print(in.readInt());
//		
//		in.close();
//		out.close();
		
		// 定义一个武将类		
		// 将以下对象使用对象流保存到文件中，并读取出来
		// 编号 名字	势力	智力  武力  统帅
		// 11 刘备   蜀国 90  85    95
		// 12 曹操   魏国 92  90    95
//		General g1 = new General(11,"刘备","蜀国",90,85,95);
//		General g2 = new General(12,"曹操","魏国",90,85,95);
//		
//		ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("e:/wujiang.txt"));
//		
//		out.writeObject(g1);
//		out.writeObject(g2);
//		
//		ObjectInputStream in = new ObjectInputStream(new FileInputStream("e:/wujiang.txt"));
//		General g3 = (General)in.readObject();
//		System.out.println(g3);
//		
//		in.close();
//		out.close();
		
		// 七. RandomAccessFile类
		
		// 11. 读取推箱关卡文件的数据地址值
		// 1. 使用RandomAccessFile类打开box.idx，使用只读方式
		// 2. 让用户输入1-100范围内数字n   （关卡编号)
		// 3. 使用seek()方法移动到4*(n-1)的位置
		// 4. 使用readInt()读取这个位置的值，打印出来
		// 5. 关闭文件
				
		// * 12 在11的基础上 
		// 1. seek移动读取地址到 11题读到的位置
		// 2. 读取一个字节,保存到x   读取一个字节,保存到y
		// 3. 跳过1个字节
		// 4. 读取一个字节,保存到mapWidth   读取一个字节,保存到mapHeight
		// 5. 打印地图信息 人物起始坐标x,y 地图大小mapWidth,mapHeiht
		// 6. 关闭文件
//		
//		// 1. 打开文件
//		RandomAccessFile f = new RandomAccessFile("e:/box.idx","r");
//		
//		Scanner sc = new Scanner(System.in);
//		System.out.println("请输入关卡编号(1-100范围内):");
//		int n = sc.nextInt();
//		f.seek(4*(n-1));
//		// 2. 读取文件
//		System.out.println("当前位置是：" + f.readInt());
//		
//		// 3. 关闭
//		f.close();
//		
//		
//		RandomAccessFile f2 = new RandomAccessFile("e:/box.idx","r");		
//		f2.seek(4*(n-1));
//		byte x = f2.readByte();
//		byte y = f2.readByte();
//		f2.readByte();
//		byte mapWidth = f2.readByte();
//		byte mapHeight = f2.readByte();
//		System.out.println("人物的起始坐标是(" + x + "," + y + ")" + "地图的大小是:" + mapWidth + "X" + mapHeight);
//		
//		// 3. 关闭
//		f2.close();

		// 八、 转换流
		// 13.
		// 	1. 准备一个gbk编码的txt文件
		// 	2. 使用转换流 读取出这个txt的内容
//		Reader r = new InputStreamReader(new FileInputStream("e:/3.txt"),"gbk");
//		char[] buf = new char[1024];
//		int len = 0;
//		
//		len = r.read(buf);
//		
//		String str = new String(buf,0,len);
//		System.out.println(str);
		
		// 九、 读取配置文件
		
		// 13. 准备一个配置文件stus.txt内容如下
		// id=101
		// name=zhangsan
		// age=18
		// 使用Properties类读取出id、name、age的值
		Properties p = new Properties();
		p.load(new FileInputStream("e:/stus.txt"));
		
		System.out.println(p.getProperty("id"));
		System.out.println(p.getProperty("name"));
		System.out.println(p.getProperty("age"));
		
	}

}
