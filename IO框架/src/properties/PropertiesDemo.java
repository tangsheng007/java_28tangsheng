package properties;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertiesDemo {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
//		testRead();
		
		testWrite();
	}

	private static void testWrite() throws IOException {
		// TODO Auto-generated method stub
		
		//1
		
		Properties p = new Properties();
				
		// 2.
		p.put("name", "lisi");
		p.put("age", "18");
				
				
		// 3.
				
		p.store(new FileOutputStream("e:/setting2.ini"),"aaa");
		
	}

	private static void testRead() throws IOException, IOException {
		// TODO Auto-generated method stub
		
		// 1.
		Properties p = new Properties();
		p.load(new FileInputStream("e:/setting.ini"));
		
		// 2.
		System.out.println(p.getProperty("username"));
		System.out.println(p.getProperty("password"));
		
		
	}

}